package com.base2art.collections;

import java.util.NoSuchElementException;

public interface ISearchableList<T>
    extends IList<T>, ISearchable<T>
{

  int indexOf(T element)
      throws NoSuchElementException;

  int indexOf(T element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(T element)
      throws NoSuchElementException;

  int lastIndexOf(T element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(T element);
}
