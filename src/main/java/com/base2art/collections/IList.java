package com.base2art.collections;

public interface IList<E> extends IRandomAccessReadonlyCollection<E>
{

  void add(E element);

  void addAt(int index, E element);

  E set(int index, E element);

  E	removeAt(int index);

  void clear();
}
