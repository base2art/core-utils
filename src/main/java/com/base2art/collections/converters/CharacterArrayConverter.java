package com.base2art.collections.converters;

import java.util.ArrayList;

public class CharacterArrayConverter
{

  public static char[] toPrimitive(ArrayList<Character> typedList)
  {
    return toPrimitive(typedList.toArray(new Character[0]));
  }

  public static char[] toPrimitive(Character[] typedList)
  {
    char[] typedArray = new char[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
