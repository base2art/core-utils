package com.base2art.collections.converters;

import java.util.ArrayList;

public class FloatArrayConverter
{

  public static float[] toPrimitive(ArrayList<Float> typedList)
  {
    return toPrimitive(typedList.toArray(new Float[0]));
  }

  public static float[] toPrimitive(Float[] typedList)
  {
    float[] typedArray = new float[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
