package com.base2art.collections.converters;

import java.util.ArrayList;

public class ShortArrayConverter
{

  public static short[] toPrimitive(ArrayList<Short> typedList)
  {
    return toPrimitive(typedList.toArray(new Short[0]));
  }

  public static short[] toPrimitive(Short[] typedList)
  {
    short[] typedArray = new short[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
