package com.base2art.collections.converters;

import java.util.ArrayList;

public class BooleanArrayConverter
{

  public static boolean[] toPrimitive(ArrayList<Boolean> typedList)
  {
    return toPrimitive(typedList.toArray(new Boolean[0]));
  }

  public static boolean[] toPrimitive(Boolean[] typedList)
  {
    boolean[] typedArray = new boolean[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
