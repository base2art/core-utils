package com.base2art.collections.converters;

import java.util.ArrayList;

public class DoubleArrayConverter
{

  public static double[] toPrimitive(ArrayList<Double> typedList)
  {
    return toPrimitive(typedList.toArray(new Double[0]));
  }

  public static double[] toPrimitive(Double[] typedList)
  {
    double[] typedArray = new double[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
