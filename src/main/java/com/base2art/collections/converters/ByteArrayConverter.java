package com.base2art.collections.converters;

import java.util.ArrayList;

public class ByteArrayConverter
{

  public static byte[] toPrimitive(ArrayList<Byte> typedList)
  {
    return toPrimitive(typedList.toArray(new Byte[0]));
  }

  public static byte[] toPrimitive(Byte[] typedList)
  {
    byte[] typedArray = new byte[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
