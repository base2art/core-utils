package com.base2art.collections.converters;

import java.util.ArrayList;

public class LongArrayConverter
{

  public static long[] toPrimitive(ArrayList<Long> typedList)
  {
    return toPrimitive(typedList.toArray(new Long[0]));
  }

  public static long[] toPrimitive(Long[] typedList)
  {
    long[] typedArray = new long[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
