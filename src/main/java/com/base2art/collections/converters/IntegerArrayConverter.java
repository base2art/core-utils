package com.base2art.collections.converters;

import java.util.ArrayList;

public class IntegerArrayConverter
{

  public static int[] toPrimitive(ArrayList<Integer> typedList)
  {
    return toPrimitive(typedList.toArray(new Integer[0]));
  }

  public static int[] toPrimitive(Integer[] typedList)
  {
    int[] typedArray = new int[typedList.length];
    for (int i = 0; i < typedList.length; i++)
    {
      typedArray[i] = typedList[i];
    }

    return typedArray;
  }
}
