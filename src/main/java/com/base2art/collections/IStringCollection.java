package com.base2art.collections;

public interface IStringCollection 
  extends  IRandomAccessReadonlyCollection<String>
{
  String join(String separator);

  @Override
  IStringCollection reverse();

  @Override
  StringCollection asList();
}
