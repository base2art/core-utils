package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface ICharSearchableList
    extends ICharList, ICharSearchable
{

  int indexOf(char element)
      throws NoSuchElementException;

  int indexOf(char element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(char element)
      throws NoSuchElementException;

  int lastIndexOf(char element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(char element);
}
