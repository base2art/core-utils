package com.base2art.collections.specialized;

import com.base2art.collections.converters.ByteArrayConverter;
import com.base2art.collections.specialized.GenericIntList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericByteList
    implements IByteSearchableList
{

  private final com.base2art.comparators.ByteComparator comparer = new com.base2art.comparators.ByteComparator();

  private final ArrayList<Byte> items = new ArrayList<Byte>();
  private final Class<Byte> klazz = Byte.class;

  public GenericByteList()
  {
    this(null);
  }

  public GenericByteList(Iterable<Byte> iterable)
  {
    if (iterable != null)
    {
      for (Byte item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(byte element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, byte element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public byte set(int i, byte element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public byte removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public byte[] toArray()
  {
    return ByteArrayConverter.toPrimitive(this.items);
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(byte element)
  {
    for (byte searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(byte element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(byte element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(byte element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(byte element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(byte element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<Byte> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public byte get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public IByteRandomAccessReadonlyCollection reverse()
  {
    final GenericByteList collection = new GenericByteList();

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      byte object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public IByteList asList()
  {
    return new GenericByteList(this);
  }

  @Override
  public byte first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public byte last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
