package com.base2art.collections.specialized;

public interface ILongSearchable
{

  boolean contains(long element);
}
