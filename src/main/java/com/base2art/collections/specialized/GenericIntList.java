package com.base2art.collections.specialized;

import com.base2art.collections.converters.IntegerArrayConverter;
import com.base2art.collections.specialized.GenericIntList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericIntList
    implements IIntSearchableList
{

  private final com.base2art.comparators.IntegerComparator comparer = new com.base2art.comparators.IntegerComparator();

  private final ArrayList<Integer> items = new ArrayList<Integer>();
  private final Class<Integer> klazz = Integer.class;

  public GenericIntList()
  {
    this(null);
  }

  public GenericIntList(Iterable<Integer> iterable)
  {
    if (iterable != null)
    {
      for (Integer item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(int element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, int element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public int set(int i, int element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public int removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public int[] toArray()
  {
    return IntegerArrayConverter.toPrimitive(this.items);
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(int element)
  {
    for (int searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(int element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(int element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(int element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(int element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(int element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<Integer> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public int get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public IIntRandomAccessReadonlyCollection reverse()
  {
    final GenericIntList collection = new GenericIntList();

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      int object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public IIntList asList()
  {
    return new GenericIntList(this);
  }

  @Override
  public int first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public int last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
