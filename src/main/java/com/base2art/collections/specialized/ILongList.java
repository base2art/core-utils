package com.base2art.collections.specialized;

public interface ILongList extends ILongRandomAccessReadonlyCollection
{

  void add(long element);

  void addAt(int index, long element);

  long set(int index, long element);

  long removeAt(int index);

  void clear();
}
