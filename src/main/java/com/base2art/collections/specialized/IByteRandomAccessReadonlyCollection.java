package com.base2art.collections.specialized;

public interface IByteRandomAccessReadonlyCollection
    extends Iterable<Byte>
{

  int size();

  byte get(int i) throws IndexOutOfBoundsException;

  IByteRandomAccessReadonlyCollection reverse();

  IByteList asList();

  byte first() throws IndexOutOfBoundsException;

  byte last() throws IndexOutOfBoundsException;

  byte[] toArray();
}
