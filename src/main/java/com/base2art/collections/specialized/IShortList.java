package com.base2art.collections.specialized;

public interface IShortList extends IShortRandomAccessReadonlyCollection
{

  void add(short element);

  void addAt(int index, short element);

  short set(int index, short element);

  short removeAt(int index);

  void clear();
}
