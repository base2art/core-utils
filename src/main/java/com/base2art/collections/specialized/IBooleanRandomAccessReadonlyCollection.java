package com.base2art.collections.specialized;

public interface IBooleanRandomAccessReadonlyCollection
    extends Iterable<Boolean>
{

  int size();

  boolean get(int i) throws IndexOutOfBoundsException;

  IBooleanRandomAccessReadonlyCollection reverse();

  IBooleanList asList();

  boolean first() throws IndexOutOfBoundsException;

  boolean last() throws IndexOutOfBoundsException;

  boolean[] toArray();
}
