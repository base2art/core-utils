package com.base2art.collections.specialized;

public interface IIntRandomAccessReadonlyCollection
    extends Iterable<Integer>
{

  int size();

  int get(int i) throws IndexOutOfBoundsException;

  IIntRandomAccessReadonlyCollection reverse();

  IIntList asList();

  int first() throws IndexOutOfBoundsException;

  int last() throws IndexOutOfBoundsException;

  int[] toArray();
}
