package com.base2art.collections.specialized;

public interface IByteList extends IByteRandomAccessReadonlyCollection
{

  void add(byte element);

  void addAt(int index, byte element);

  byte set(int index, byte element);

  byte removeAt(int index);

  void clear();
}
