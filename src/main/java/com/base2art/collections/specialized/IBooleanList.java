package com.base2art.collections.specialized;

public interface IBooleanList extends IBooleanRandomAccessReadonlyCollection
{

  void add(boolean element);

  void addAt(int index, boolean element);

  boolean set(int index, boolean element);

  boolean removeAt(int index);

  void clear();
}
