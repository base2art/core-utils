package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface IBooleanSearchableList
    extends IBooleanList, IBooleanSearchable
{

  int indexOf(boolean element)
      throws NoSuchElementException;

  int indexOf(boolean element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(boolean element)
      throws NoSuchElementException;

  int lastIndexOf(boolean element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(boolean element);
}
