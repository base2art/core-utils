package com.base2art.collections.specialized;

public interface IIntSearchable
{

  boolean contains(int element);
}
