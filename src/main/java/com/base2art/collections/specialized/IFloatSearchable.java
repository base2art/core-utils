package com.base2art.collections.specialized;

public interface IFloatSearchable
{

  boolean contains(float element);
}
