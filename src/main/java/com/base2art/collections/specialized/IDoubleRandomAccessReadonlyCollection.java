package com.base2art.collections.specialized;

public interface IDoubleRandomAccessReadonlyCollection
    extends Iterable<Double>
{

  int size();

  double get(int i) throws IndexOutOfBoundsException;

  IDoubleRandomAccessReadonlyCollection reverse();

  IDoubleList asList();

  double first() throws IndexOutOfBoundsException;

  double last() throws IndexOutOfBoundsException;

  double[] toArray();
}
