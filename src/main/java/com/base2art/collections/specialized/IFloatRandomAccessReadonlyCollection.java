package com.base2art.collections.specialized;

public interface IFloatRandomAccessReadonlyCollection
    extends Iterable<Float>
{

  int size();

  float get(int i) throws IndexOutOfBoundsException;

  IFloatRandomAccessReadonlyCollection reverse();

  IFloatList asList();

  float first() throws IndexOutOfBoundsException;

  float last() throws IndexOutOfBoundsException;

  float[] toArray();
}
