package com.base2art.collections.specialized;

public interface IShortRandomAccessReadonlyCollection
    extends Iterable<Short>
{

  int size();

  short get(int i) throws IndexOutOfBoundsException;

  IShortRandomAccessReadonlyCollection reverse();

  IShortList asList();

  short first() throws IndexOutOfBoundsException;

  short last() throws IndexOutOfBoundsException;

  short[] toArray();
}
