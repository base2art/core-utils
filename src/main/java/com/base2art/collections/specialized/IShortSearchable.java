package com.base2art.collections.specialized;

public interface IShortSearchable
{

  boolean contains(short element);
}
