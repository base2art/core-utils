package com.base2art.collections.specialized;

public interface IDoubleSearchable
{

  boolean contains(double element);
}
