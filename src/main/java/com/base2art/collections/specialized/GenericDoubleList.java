package com.base2art.collections.specialized;

import com.base2art.collections.converters.DoubleArrayConverter;
import com.base2art.collections.specialized.GenericIntList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericDoubleList
    implements IDoubleSearchableList
{

  private final com.base2art.comparators.DoubleComparator comparer = new com.base2art.comparators.DoubleComparator();

  private final ArrayList<Double> items = new ArrayList<Double>();
  private final Class<Double> klazz = Double.class;

  public GenericDoubleList()
  {
    this(null);
  }

  public GenericDoubleList(Iterable<Double> iterable)
  {
    if (iterable != null)
    {
      for (Double item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(double element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, double element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public double set(int i, double element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public double removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public double[] toArray()
  {
    return DoubleArrayConverter.toPrimitive(this.items);
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(double element)
  {
    for (double searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(double element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(double element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(double element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(double element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(double element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<Double> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public double get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public IDoubleRandomAccessReadonlyCollection reverse()
  {
    final GenericDoubleList collection = new GenericDoubleList();

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      double object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public IDoubleList asList()
  {
    return new GenericDoubleList(this);
  }

  @Override
  public double first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public double last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
