package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface IIntSearchableList
    extends IIntList, IIntSearchable
{

  int indexOf(int element)
      throws NoSuchElementException;

  int indexOf(int element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(int element)
      throws NoSuchElementException;

  int lastIndexOf(int element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(int element);
}
