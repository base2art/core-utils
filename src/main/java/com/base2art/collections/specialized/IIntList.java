package com.base2art.collections.specialized;

public interface IIntList extends IIntRandomAccessReadonlyCollection
{

  void add(int element);

  void addAt(int index, int element);

  int set(int index, int element);

  int removeAt(int index);

  void clear();
}
