package com.base2art.collections.specialized;

public interface ICharList extends ICharRandomAccessReadonlyCollection
{

  void add(char element);

  void addAt(int index, char element);

  char set(int index, char element);

  char removeAt(int index);

  void clear();
}
