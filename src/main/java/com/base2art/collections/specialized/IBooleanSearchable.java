package com.base2art.collections.specialized;

public interface IBooleanSearchable
{

  boolean contains(boolean element);
}
