package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface IByteSearchableList
    extends IByteList, IByteSearchable
{

  int indexOf(byte element)
      throws NoSuchElementException;

  int indexOf(byte element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(byte element)
      throws NoSuchElementException;

  int lastIndexOf(byte element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(byte element);
}
