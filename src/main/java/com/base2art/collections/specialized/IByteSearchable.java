package com.base2art.collections.specialized;

public interface IByteSearchable
{

  boolean contains(byte element);
}
