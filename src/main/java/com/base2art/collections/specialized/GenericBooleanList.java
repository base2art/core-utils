package com.base2art.collections.specialized;

import com.base2art.collections.converters.BooleanArrayConverter;
import com.base2art.collections.specialized.GenericIntList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericBooleanList
    implements IBooleanSearchableList
{

  private final com.base2art.comparators.BooleanComparator comparer = new com.base2art.comparators.BooleanComparator();

  private final ArrayList<Boolean> items = new ArrayList<Boolean>();
  private final Class<Boolean> klazz = Boolean.class;

  public GenericBooleanList()
  {
    this(null);
  }

  public GenericBooleanList(Iterable<Boolean> iterable)
  {
    if (iterable != null)
    {
      for (Boolean item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(boolean element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, boolean element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public boolean set(int i, boolean element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public boolean removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public boolean[] toArray()
  {
    return BooleanArrayConverter.toPrimitive(this.items);
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(boolean element)
  {
    for (boolean searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(boolean element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(boolean element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(boolean element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(boolean element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(boolean element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<Boolean> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public boolean get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public IBooleanRandomAccessReadonlyCollection reverse()
  {
    final GenericBooleanList collection = new GenericBooleanList();

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      boolean object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public IBooleanList asList()
  {
    return new GenericBooleanList(this);
  }

  @Override
  public boolean first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public boolean last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
