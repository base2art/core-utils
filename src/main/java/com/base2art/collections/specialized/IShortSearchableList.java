package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface IShortSearchableList
    extends IShortList, IShortSearchable
{

  int indexOf(short element)
      throws NoSuchElementException;

  int indexOf(short element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(short element)
      throws NoSuchElementException;

  int lastIndexOf(short element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(short element);
}
