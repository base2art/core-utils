package com.base2art.collections.specialized;

import com.base2art.collections.converters.FloatArrayConverter;
import com.base2art.collections.specialized.GenericIntList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericFloatList
    implements IFloatSearchableList
{

  private final com.base2art.comparators.FloatComparator comparer = new com.base2art.comparators.FloatComparator();

  private final ArrayList<Float> items = new ArrayList<Float>();
  private final Class<Float> klazz = Float.class;

  public GenericFloatList()
  {
    this(null);
  }

  public GenericFloatList(Iterable<Float> iterable)
  {
    if (iterable != null)
    {
      for (Float item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(float element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, float element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public float set(int i, float element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public float removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public float[] toArray()
  {
    return FloatArrayConverter.toPrimitive(this.items);
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(float element)
  {
    for (float searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(float element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(float element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(float element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(float element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(float element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<Float> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public float get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public IFloatRandomAccessReadonlyCollection reverse()
  {
    final GenericFloatList collection = new GenericFloatList();

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      float object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public IFloatList asList()
  {
    return new GenericFloatList(this);
  }

  @Override
  public float first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public float last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
