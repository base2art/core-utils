package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface IFloatSearchableList
    extends IFloatList, IFloatSearchable
{

  int indexOf(float element)
      throws NoSuchElementException;

  int indexOf(float element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(float element)
      throws NoSuchElementException;

  int lastIndexOf(float element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(float element);
}
