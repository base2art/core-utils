package com.base2art.collections.specialized;

import com.base2art.collections.converters.CharacterArrayConverter;
import com.base2art.collections.specialized.GenericIntList;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericCharList
    implements ICharSearchableList
{

  private final com.base2art.comparators.CharacterComparator comparer = new com.base2art.comparators.CharacterComparator();

  private final ArrayList<Character> items = new ArrayList<Character>();
  private final Class<Character> klazz = Character.class;

  public GenericCharList()
  {
    this(null);
  }

  public GenericCharList(Iterable<Character> iterable)
  {
    if (iterable != null)
    {
      for (Character item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(char element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, char element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public char set(int i, char element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public char removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public char[] toArray()
  {
    return CharacterArrayConverter.toPrimitive(this.items);
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(char element)
  {
    for (char searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(char element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(char element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(char element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(char element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(char element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<Character> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public char get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public ICharRandomAccessReadonlyCollection reverse()
  {
    final GenericCharList collection = new GenericCharList();

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      char object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public ICharList asList()
  {
    return new GenericCharList(this);
  }

  @Override
  public char first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public char last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
