package com.base2art.collections.specialized;

public interface ILongRandomAccessReadonlyCollection
    extends Iterable<Long>
{

  int size();

  long get(int i) throws IndexOutOfBoundsException;

  ILongRandomAccessReadonlyCollection reverse();

  ILongList asList();

  long first() throws IndexOutOfBoundsException;

  long last() throws IndexOutOfBoundsException;

  long[] toArray();
}
