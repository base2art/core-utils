package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface IDoubleSearchableList
    extends IDoubleList, IDoubleSearchable
{

  int indexOf(double element)
      throws NoSuchElementException;

  int indexOf(double element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(double element)
      throws NoSuchElementException;

  int lastIndexOf(double element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(double element);
}
