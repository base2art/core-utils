package com.base2art.collections.specialized;

public interface ICharSearchable
{

  boolean contains(char element);
}
