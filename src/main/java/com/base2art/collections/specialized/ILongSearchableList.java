package com.base2art.collections.specialized;

import java.util.NoSuchElementException;

public interface ILongSearchableList
    extends ILongList, ILongSearchable
{

  int indexOf(long element)
      throws NoSuchElementException;

  int indexOf(long element, boolean suppressException)
      throws NoSuchElementException;

  int lastIndexOf(long element)
      throws NoSuchElementException;

  int lastIndexOf(long element, boolean suppressException)
      throws NoSuchElementException;

  int[] remove(long element);
}
