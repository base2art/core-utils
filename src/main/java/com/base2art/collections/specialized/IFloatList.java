package com.base2art.collections.specialized;

public interface IFloatList extends IFloatRandomAccessReadonlyCollection
{

  void add(float element);

  void addAt(int index, float element);

  float set(int index, float element);

  float removeAt(int index);

  void clear();
}
