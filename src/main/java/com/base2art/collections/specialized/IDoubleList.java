package com.base2art.collections.specialized;

public interface IDoubleList extends IDoubleRandomAccessReadonlyCollection
{

  void add(double element);

  void addAt(int index, double element);

  double set(int index, double element);

  double removeAt(int index);

  void clear();
}
