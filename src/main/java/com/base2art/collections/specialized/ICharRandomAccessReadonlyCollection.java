package com.base2art.collections.specialized;

public interface ICharRandomAccessReadonlyCollection
    extends Iterable<Character>
{

  int size();

  char get(int i) throws IndexOutOfBoundsException;

  ICharRandomAccessReadonlyCollection reverse();

  ICharList asList();

  char first() throws IndexOutOfBoundsException;

  char last() throws IndexOutOfBoundsException;

  char[] toArray();
}
