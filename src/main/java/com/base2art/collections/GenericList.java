package com.base2art.collections;

import com.base2art.collections.specialized.GenericIntList;
import com.base2art.comparators.ReferenceComparator;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericList<T>
    implements ISearchableList<T>
{

  private final java.util.Comparator<T> comparer;

  private final ArrayList<T> items = new ArrayList<T>();
  private final Class<T> klazz;

  public GenericList(Class<T> klazz)
  {
    this(klazz, null, null);
  }

  public GenericList(Class<T> klazz, Iterable<T> iterable)
  {
    this(klazz, iterable, null);
  }

  public GenericList(Class<T> klazz, java.util.Comparator<T> comparer)
  {
    this(klazz, null, comparer);
  }

  public GenericList(Class<T> klazz, Iterable<T> iterable, java.util.Comparator<T> comparer)
  {
    this.klazz = klazz;
    this.comparer = comparer != null ? comparer : new ReferenceComparator<T>();

    if (iterable != null)
    {
      for (T item : iterable)
      {
        this.items.add(item);
      }
    }
  }

  // LIST INTERFACE
  @Override
  public void add(T element)
  {
    this.items.add(element);
  }

  @Override
  public void addAt(int index, T element)
      throws IndexOutOfBoundsException
  {
    this.items.add(index, element);
  }

  @Override
  public T set(int i, T element)
      throws IndexOutOfBoundsException
  {
    return this.items.set(i, element);
  }

  @Override
  public T removeAt(int index)
  {
    return this.items.remove(index);
  }

  @Override
  public void clear()
  {
    this.items.clear();
  }

  @Override
  public T[] toArray()
  {
    return (T[]) this.items.toArray((T[]) Array.newInstance(this.klazz, 0));
  }

  // SEARCHABLE INTERFACE
  @Override
  public boolean contains(T element)
  {
    for (T searchValue : this.items)
    {
      if (this.comparer.compare(searchValue, element) == 0)
      {
        return true;
      }
    }

    return false;
  }

  // SEARCHABLE LIST INTERFACE
  @Override
  public int indexOf(T element)
      throws NoSuchElementException
  {
    return this.indexOf(element, false);
  }

  @Override
  public int indexOf(T element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = 0; i < this.items.size(); i++)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int lastIndexOf(T element)
      throws NoSuchElementException
  {
    return this.lastIndexOf(element, false);
  }

  @Override
  public int lastIndexOf(T element, boolean suppressException)
      throws NoSuchElementException
  {
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        return i;
      }
    }

    if (suppressException)
    {
      return -1;
    }
    else
    {
      throw new NoSuchElementException();
    }
  }

  @Override
  public int[] remove(T element)
  {
    GenericIntList list = new GenericIntList();
    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      if (this.comparer.compare(this.items.get(i), element) == 0)
      {
        list.addAt(0, i);
      }
    }
    
    return list.toArray();
  }

  // READONLY INTERFACE
  @Override
  public Iterator<T> iterator()
  {
    return this.items.iterator();
  }

  @Override
  public int size()
  {
    return this.items.size();
  }

  @Override
  public T get(int i)
  {
    return this.items.get(i);
  }

  @Override
  public IRandomAccessReadonlyCollection<T> reverse()
  {
    final GenericList<T> collection = new GenericList<T>(this.klazz);

    for (int i = this.items.size() - 1; i >= 0; i--)
    {
      T object = this.items.get(i);
      collection.add(object);
    }

    return collection;
  }

  @Override
  public IList<T> asList()
  {
    return new GenericList<T>(this.klazz, this);
  }

  @Override
  public T first() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(0);
  }

  @Override
  public T last() throws IndexOutOfBoundsException
  {
    if (this.items.size() <= 0)
    {
      throw new IndexOutOfBoundsException("No Elements in collection");
    }

    return this.items.get(items.size() - 1);
  }
}
