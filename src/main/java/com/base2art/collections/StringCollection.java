package com.base2art.collections;

public class StringCollection
    extends GenericList<String>
    implements IStringCollection
{

  public StringCollection()
  {
    super(String.class);
  }

  public StringCollection(Iterable<String> iterable)
  {
    super(String.class, iterable);
  }

  @Override
  public String join(String separator)
  {
    if (separator == null)
    {
      separator = "";
    }

    StringBuilder sb = new StringBuilder();
    final int size = this.size();

    if (size == 0)
    {
      return sb.toString();
    }

    sb.append(this.get(0));

    for (int i = 1; i < size; i++)
    {
      sb.append(separator);
      sb.append(this.get(i));
    }

    return sb.toString();
  }

  @Override
  public IStringCollection reverse()
  {
    return new StringCollection(super.reverse());
  }

  @Override
  public StringCollection asList()
  {
    return new StringCollection(this);
  }
}
