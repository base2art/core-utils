package com.base2art.collections;

public interface IRandomAccessReadonlyCollection<T>
    extends Iterable<T>
{

  int size();

  T get(int i) throws IndexOutOfBoundsException;

  IRandomAccessReadonlyCollection<T> reverse();

  IList<T> asList();

  T first() throws IndexOutOfBoundsException;

  T last() throws IndexOutOfBoundsException;

  T[] toArray();
}
