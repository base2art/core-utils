package com.base2art.collections;

public interface ISearchable<T>
{

  boolean contains(T element);
}
