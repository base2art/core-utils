package com.base2art.lang;

public class ObjectUtil
{

  public static <T> T as(Object obj, Class<T> klazz)
  {
    if (klazz.isAssignableFrom(obj.getClass()))
    {
      return (T) obj;
    }

    return null;
  }
}
