package com.base2art.lang;

import com.base2art.collections.StringCollection;
import org.atteo.evo.inflector.English;

public final class StringFormatter
{

  public static String format(String value, StringFormatType formatValue)
  {
    return format(value, formatValue, false);
  }

  public static String formatPlural(String value, StringFormatType formatValue)
  {
    return format(value, formatValue, true);
  }

  public static String format(String value, StringFormatType formatValue, boolean pluaralize)
  {
    StringCollection parts = StringUtil.words(value).asList();

    boolean upperFirst = formatValue == StringFormatType.Pascal
      || formatValue == StringFormatType.TitleWords;

    boolean upperRest = upperFirst
      || formatValue == StringFormatType.Camel;

    boolean lowerMain = formatValue != StringFormatType.Words;

    String separator = "";

    if (formatValue == StringFormatType.Underscore)
    {
      separator = "_";
    }

    if (formatValue == StringFormatType.Dash)
    {
      separator = "-";
    }

    if (formatValue == StringFormatType.Words || formatValue == StringFormatType.TitleWords)
    {
      separator = " ";
    }

    if (pluaralize)
    {
      final int listIdx = parts.size() - 1;
      String last = parts.get(listIdx);
      last = English.plural(last);
      parts.set(listIdx, last);
    }

    fixItem(parts, 0, upperFirst, lowerMain);
    for (int i = 1; i < parts.size(); i++)
    {
      fixItem(parts, i, upperRest, lowerMain);
    }

    return parts.join(separator);
  }

  private static void fixItem(StringCollection parts, int index, boolean shouldUpper, boolean lowerMain)
  {
    if (parts.size() <= index)
    {
      return;
    }

    String part = parts.get(index);

    if (lowerMain)
    {
      part = part.toLowerCase();
    }

    String pre = part.substring(0, 1);
    if (shouldUpper)
    {
      pre = pre.toUpperCase();
    }

    String post = part.substring(1);
    parts.set(index, pre + post);
  }
}
