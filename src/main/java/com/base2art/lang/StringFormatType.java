package com.base2art.lang;

public enum StringFormatType
{

  Pascal,
  Camel,
  Underscore,
  Dash,
  TitleWords,
  Words
}
