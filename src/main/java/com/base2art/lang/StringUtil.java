package com.base2art.lang;

import com.base2art.collections.IStringCollection;
import com.base2art.collections.StringCollection;
import java.util.ArrayList;
import java.util.StringTokenizer;

public final class StringUtil
{

  public static IStringCollection split(String input, char token)
  {
    final String tokenString = new String(new char[]
      {
        token
      });

    StringTokenizer tokens = new StringTokenizer(input, tokenString);
    StringCollection stringColl = new StringCollection();
    while (tokens.hasMoreTokens())
    {
      stringColl.add(tokens.nextToken());
    }

    return stringColl;
  }

  public static boolean hasValue(String input)
  {
    return input != null && input.trim().length() > 0;
  }

  public static IStringCollection words(String value)
  {
    StringCollection parts = new StringCollection();
    String current = "";
    for (int i = 0; i < value.length(); i++)
    {
      char item = value.charAt(i);
      if (Character.isLowerCase(item) || Character.isDigit(item))
      {
        current += Character.toString(item);
      }
      else if (Character.isLetter(item))
      {
        if (!current.equals(""))
        {
          parts.add(current);
        }

        current = Character.toString(item);
      }
      else
      {
        if (!current.equals(""))
        {
          parts.add(current);
        }

        current = "";
      }
    }

    if (!current.equals(""))
    {
      parts.add(current);
    }

    return parts;
  }
}
