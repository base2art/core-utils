package com.base2art.comparators;

import java.util.Comparator;

public class BooleanComparator
    implements Comparator<Boolean>
{

  @Override
  public int compare(Boolean t, Boolean t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
