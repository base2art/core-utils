package com.base2art.comparators;

import java.util.Comparator;

public class StringComparator
    implements Comparator<String>
{

  @Override
  public int compare(String t, String t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
