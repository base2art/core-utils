package com.base2art.comparators;

import java.util.Comparator;

public class DoubleComparator
    implements Comparator<Double>
{

  @Override
  public int compare(Double t, Double t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
