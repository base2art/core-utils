package com.base2art.comparators;

import java.util.Comparator;

public class IntegerComparator
    implements Comparator<Integer>
{

  @Override
  public int compare(Integer t, Integer t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
