package com.base2art.comparators;

public class ReferenceComparator<T>
    implements java.util.Comparator<T>
{

  public ReferenceComparator()
  {
  }

  @Override
  public int compare(T t, T t1)
  {
    
    if(t == null && t1 != null)
    {
      return 1;
    }
    
    if(t1 == null && t != null)
    {
      return -1;
    }
    
    if (t == t1)
    {
      return 0;
    }
    
    return -1;
  }
}
