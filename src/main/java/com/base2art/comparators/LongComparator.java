package com.base2art.comparators;

import java.util.Comparator;

public class LongComparator
    implements Comparator<Long>
{

  @Override
  public int compare(Long t, Long t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
