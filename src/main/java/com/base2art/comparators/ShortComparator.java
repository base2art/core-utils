package com.base2art.comparators;

import java.util.Comparator;

public class ShortComparator
    implements Comparator<Short>
{

  @Override
  public int compare(Short t, Short t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
