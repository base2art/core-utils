package com.base2art.comparators;

import java.util.Comparator;

public class ByteComparator
    implements Comparator<Byte>
{

  @Override
  public int compare(Byte t, Byte t1)
  {
    if (t == null && t1 == null)
    {
      return 0;
    }

    if (t == null)
    {
      return -1;
    }

    return t.compareTo(t1);
  }
}
