package com.base2art.util;

public interface Option<T>
{
  T get();
  T getOrElse(T value);
  boolean isNone();
}
