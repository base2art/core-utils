package com.base2art.util.parsing;

import java.util.HashMap;
import java.util.Map;

public class Parsibility
{

  private final static Map<Class<?>, IParser<?>> parsers;

  static
  {
    parsers = new HashMap<Class<?>, IParser<?>>();
    parsers.put(Integer.class, new IntegerParser());
  }

  public static <T> IParser<T> findParser(Class<T> klazz)
  {
    return (IParser<T>) parsers.get(klazz);
  }

  public static <T> void registerParser(Class<T> klazz, IParser<T> parser)
  {
    parsers.put(klazz, parser);
  }
}
