package com.base2art.util.parsing;

import com.base2art.util.Option;
import com.base2art.util.OptionCreator;

public class IntegerParser implements IParser<Integer>
{

  @Override
  public Integer parse(String value)
  {
    return Integer.parseInt(value);
  }

  @Override
  public Class<Integer> getTargetClass()
  {
    return Integer.class;
  }

  @Override
  public Option<Integer> tryParse(String value)
  {
    try
    {
      return OptionCreator.<Integer>some(this.parse(value));
    }
    catch (NumberFormatException nfe)
    {
      return OptionCreator.<Integer>none();
    }
  }
}
