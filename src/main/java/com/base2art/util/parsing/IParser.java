package com.base2art.util.parsing;

import com.base2art.util.Option;

public interface IParser<T>
{

  Option<T> tryParse(String value);

  T parse(String value) throws ParsingException;

  Class<T> getTargetClass();
}
