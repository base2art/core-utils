package com.base2art.util.parsing;


class ParsingException extends RuntimeException {

  public ParsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
  {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public ParsingException(Throwable cause)
  {
    super(cause);
  }

  public ParsingException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public ParsingException(String message)
  {
    super(message);
  }

  public ParsingException()
  {
  }

}
