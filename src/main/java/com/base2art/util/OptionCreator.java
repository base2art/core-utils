package com.base2art.util;

import java.util.NoSuchElementException;

public class OptionCreator
{

  public static <T> Option<T> none()
  {
    return new Option<T>()
    {

      @Override
      public T get()
      {
        throw new NoSuchElementException("Option get() is none.");
      }

      @Override
      public T getOrElse(T value)
      {
        return value;
      }

      @Override
      public boolean isNone()
      {
        return true;
      }
    };
  }

  public static <T> Option<T> some(final T value)
  {

    return new Option<T>()
    {

      final T val = value;

      @Override
      public T get()
      {
        return this.val;
      }

      @Override
      public T getOrElse(T value)
      {
        return this.val;
      }

      @Override
      public boolean isNone()
      {
        return false;
      }
    };
  }
}
