package com.base2art.util;

public interface IValueContainer<T>
{

  public void setValue(T value);
  public T getValue();
}
