package com.base2art.util.reflect;

public class ReflectiveClassHolder<T>
{

  private final ClassLoader classloader;
  private final String className;
  private final Class<T> klazz;
  private final boolean returnNullOnError;

  public ReflectiveClassHolder(ClassLoader classloader, String className, boolean returnNullOnError)
  {
    this.classloader = classloader;
    this.className = className;
    this.klazz = null;
    this.returnNullOnError = returnNullOnError;
  }

  public ReflectiveClassHolder(Class<T> klazz, boolean returnNullOnError)
  {
    this.classloader = null;
    this.className = null;
    this.klazz = klazz;
    this.returnNullOnError = returnNullOnError;
  }

  public ReflectiveClassHolder(ReflectiveClassHolder<?> holder, Class<T> klazz)
  {
    this.classloader = holder.classloader;
    this.className = null;
    this.klazz = klazz;
    this.returnNullOnError = holder.returnNullOnError;
  }

  public boolean isReturnNullOnError()
  {
    return returnNullOnError;
  }

  public Class<T> getType()
  {
    if (this.klazz != null)
    {
      return this.klazz;
    }

    return this.getKlazzByLoader();
  }

  private Class<T> getKlazzByLoader()
  {
    try
    {
      return (Class<T>)this.classLoader(this.classloader).loadClass(this.className);
    }
    catch (SecurityException ex)
    {
      if (returnNullOnError)
      {
        return null;
      }

      throw new RuntimeException("Class Not Found: '" + className + "'", ex);
    }
    catch (ClassNotFoundException ex)
    {
      if (returnNullOnError)
      {
        return null;
      }

      throw new RuntimeException("Class Not Found: '" + className + "'", ex);
    }
  }

  private ClassLoader classLoader(ClassLoader loader)
  {
    if (loader != null)
    {
      return loader;
    }

    return ReflectiveClassHolder.class.getClassLoader();
  }
}
