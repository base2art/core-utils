package com.base2art.util.reflect;

public interface IReflectiveMethod<TParent, TResult>
{

  TResult invoke();

  TResult invokeWith(Object... args);

  IReflectiveClass<TResult> returnType();

  IReflectiveClass<TResult> returnTypeWith(Object... args);
}
