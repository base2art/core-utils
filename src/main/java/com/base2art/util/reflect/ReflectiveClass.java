package com.base2art.util.reflect;

public class ReflectiveClass<T>
  implements IReflectiveClass<T>
{

  private final ReflectiveClassHolder<T> holder;

  ReflectiveClass(ReflectiveClassHolder<T> holder)
  {
    this.holder = holder;
  }

  @Override
  public T createInstance()
  {
    return new ReflectiveInstance<T>(this.holder).newInstance();
  }

  @Override
  public <TReturn> IReflectiveMethod<T, TReturn> method(String methodName)
  {
    return this.method(null, methodName);
  }

  @Override
  public <TReturn> IReflectiveMethod<T, TReturn> methodOfNew(String methodName)
  {
    return this.method(this.createInstance(), methodName);
  }

  @Override
  public <TReturn> IReflectiveMethod<T, TReturn> method(T instance, String methodName)
  {
    return new ReflectiveMethod<T, TReturn>(this.holder, instance, methodName);
  }

  @Override
  public <TReturn> IReflectiveField<T, TReturn> field(String fieldName)
  {
    return this.field(null, fieldName);
  }

  @Override
  public <TReturn> IReflectiveField<T, TReturn> fieldOfNew(String fieldName)
  {
    return this.field(this.createInstance(), fieldName);
  }

  @Override
  public <TReturn> IReflectiveField<T, TReturn> field(T instance, String fieldName)
  {
    return new ReflectiveField<T, TReturn>(this.holder, instance, fieldName);
  }
}
