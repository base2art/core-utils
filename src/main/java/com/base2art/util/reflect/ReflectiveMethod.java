package com.base2art.util.reflect;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class ReflectiveMethod<TParent, TReturn>
    extends ReflectiveNamedBase<TParent, TReturn>
    implements IReflectiveMethod<TParent, TReturn>
{

  private final TParent instance;

  protected ReflectiveMethod(ReflectiveClassHolder<TParent> holder, TParent instance, String methodName)
  {
    super(holder, methodName);
    this.instance = instance;
  }

  @Override
  public TReturn invoke()
  {
    return this.invokeInternal(this.getInvoker());
  }

  @Override
  public TReturn invokeWith(final Object... args)
  {
    return this.invokeInternal(this.getArgsInvoker(args));
  }

  @Override
  public IReflectiveClass<TReturn> returnType()
  {
    return this.<IReflectiveClass<TReturn>>invokeGeneric(this.getReturnTypeInvoker());
  }

  @Override
  public IReflectiveClass<TReturn> returnTypeWith(Object... args)
  {
    return this.<IReflectiveClass<TReturn>>invokeGeneric(this.getReturnTypeArgsInvoker(args));
  }

  protected Method getMethodNoArgs() throws NoSuchMethodException
  {
    final Class<TParent> klazz = getHolder().getType();
    if (klazz != null || !this.getHolder().isReturnNullOnError())
    {
      return klazz.getMethod(this.getName());
    }
    else
    {
      return null;
    }
  }

  protected Method getMethodWithArgs(Object... args) throws
      NoSuchMethodException
  {
    final Class<?> klazz = this.getHolder().getType();
    final String methodName = this.getName();

    ArrayList<Class<?>> items = new ArrayList<Class<?>>();
    boolean hasNullItem = true;
    for (Object object : args)
    {
      if (object != null)
      {
        items.add(object.getClass());
      }
      else
      {
        hasNullItem = true;
      }
    }

    if (hasNullItem)
    {

      final Method[] methods = klazz.getMethods();
      for (Method method : methods)
      {
        if (methodName.equals(method.getName())
            && method.getParameterTypes().length == args.length)
        {
          return method;
        }
      }

      returnOrThrow(new NoSuchMethodException("Method with Arg Count Not Found"));
    }

    return klazz.getMethod(methodName, items.toArray(new Class<?>[0]));
  }

  protected IReflectiveCallable<TReturn> getInvoker()
  {
    final ReflectiveMethod<TParent, TReturn> parentMethod = this;
    return new IReflectiveCallable<TReturn>()
    {

      @Override
      public TReturn call() throws ReflectiveOperationException
      {
        final Method method = getMethodNoArgs();

        if (method == null && parentMethod.isReturnNullOnError())
        {
          return null;
        }

        return (TReturn) method.invoke(instance);
      }
    };
  }

  protected IReflectiveCallable<TReturn> getArgsInvoker(final Object[] args)
  {
    final ReflectiveMethod<TParent, TReturn> parentMethod = this;

    return new IReflectiveCallable<TReturn>()
    {
      @Override
      public TReturn call() throws ReflectiveOperationException
      {
        final Method method = getMethodWithArgs(args);

        if (method == null && parentMethod.isReturnNullOnError())
        {
          return null;
        }

        return (TReturn) method.invoke(instance, args);
      }
    };
  }

  protected IReflectiveCallable<IReflectiveClass<TReturn>> getReturnTypeInvoker()
  {
    final ReflectiveMethod<TParent, TReturn> parentMethod = this;

    return new IReflectiveCallable<IReflectiveClass<TReturn>>()
    {

      @Override
      public IReflectiveClass<TReturn> call() throws
          ReflectiveOperationException
      {
        final Method method = getMethodNoArgs();

        if (method == null && parentMethod.isReturnNullOnError())
        {
          return null;
        }

        return Reflective.forClass(getHolder(), (Class<TReturn>) method.getReturnType());
      }
    };
  }

  protected IReflectiveCallable<IReflectiveClass<TReturn>> getReturnTypeArgsInvoker(final Object[] args)
  {
    final ReflectiveMethod<TParent, TReturn> parentMethod = this;

    return new IReflectiveCallable<IReflectiveClass<TReturn>>()
    {

      @Override
      public IReflectiveClass<TReturn> call() throws
          ReflectiveOperationException
      {
        final Method method = getMethodWithArgs(args);

        if (method == null && parentMethod.isReturnNullOnError())
        {
          return null;
        }

        return Reflective.forClass(getHolder(), (Class<TReturn>) method.getReturnType());
      }
    };
  }
}
