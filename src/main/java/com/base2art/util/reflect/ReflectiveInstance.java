package com.base2art.util.reflect;

import com.base2art.annotations.DefaultImpl;

public class ReflectiveInstance<T>
  extends ReflectiveBase<T>
{

  public ReflectiveInstance(ReflectiveClassHolder<T> holder)
  {
    super(holder);
  }

  public T newInstance()
  {
    try
    {
      final Class<T> type = this.getHolder().getType();
      if (type.isInterface())
      {
        DefaultImpl di = type.getAnnotation(DefaultImpl.class);
        if (di != null)
        {
          return (T) di.value().newInstance();
        }
        else
        {
          return type.newInstance();
        }
      }
      else
      {
        return type.newInstance();
      }
    }
    catch (InstantiationException ex)
    {
      return this.returnOrThrow(new RuntimeException("Error Creating Type", ex));
    }
    catch (IllegalAccessException ex)
    {
      return this.returnOrThrow(new RuntimeException("Error Creating Type", ex));
    }
    catch (ClassCastException ex)
    {
      return this.returnOrThrow(new RuntimeException("Error Creating Type", ex));
    }
  }
}
