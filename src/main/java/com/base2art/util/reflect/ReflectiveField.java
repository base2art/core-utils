package com.base2art.util.reflect;

import java.lang.reflect.Field;

public class ReflectiveField<TParent, TReturn>
  extends ReflectiveNamedBase<TParent, TReturn>
  implements IReflectiveField<TParent, TReturn>
{

  private final TParent instance;

  public ReflectiveField(ReflectiveClassHolder<TParent> holder, TParent instance, String methodName)
  {
    super(holder, methodName);
    this.instance = instance;
  }

  @Override
  public TReturn get()
  {
    return this.invokeInternal(this.getInvoker());
  }

  @Override
  public IReflectiveClass<TReturn> type()
  {
    return this.<IReflectiveClass<TReturn>>invokeGeneric(this.getTypeInvoker());
  }

  protected IReflectiveCallable<TReturn> getInvoker()
  {
    return new IReflectiveCallable<TReturn>()
    {

      @Override
      public TReturn call() throws ReflectiveOperationException
      {
        final Class<TParent> klazz = getHolder().getType();
        return (TReturn) klazz.getField(getName()).get(instance);
      }
    };
  }

  protected IReflectiveCallable<IReflectiveClass<TReturn>> getTypeInvoker()
  {
    return new IReflectiveCallable<IReflectiveClass<TReturn>>()
    {

      @Override
      public IReflectiveClass<TReturn> call() throws ReflectiveOperationException
      {
        final Class<TParent> klazz = getHolder().getType();
        final Field field = klazz.getField(getName());
        return Reflective.<TReturn>forClass(getHolder(), (Class<TReturn>) field.getType());
      }
    };
  }
}
