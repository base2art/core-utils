package com.base2art.util.reflect;

public interface IReflectiveClass<T>
{

  <TReturn> IReflectiveField<T, TReturn> field(String fieldName);

  <TReturn> IReflectiveField<T, TReturn> field(T instance, String fieldName);

  <TReturn> IReflectiveField<T, TReturn> fieldOfNew(String fieldName);

  <TReturn> IReflectiveMethod<T, TReturn> method(String methodName);

  <TReturn> IReflectiveMethod<T, TReturn> method(T instance, String methodName);

  <TReturn> IReflectiveMethod<T, TReturn> methodOfNew(String methodName);

  T createInstance();
}
