package com.base2art.util.reflect;

public interface IReflectiveCallable<T>
{

  T call() throws ReflectiveOperationException;
}
