package com.base2art.util.reflect;

public class ReflectiveNamedBase<TParent, TReturn>
  extends ReflectiveBase<TParent>
{

  private final String name;

  protected ReflectiveNamedBase(ReflectiveClassHolder<TParent> holder, String name)
  {
    super(holder);
    this.name = name;
  }

  protected final String getName()
  {
    return this.name;
  }

  protected TReturn invokeInternal(IReflectiveCallable<TReturn> callable)
  {
    return this.<TReturn>invokeGeneric(callable);
  }
}
