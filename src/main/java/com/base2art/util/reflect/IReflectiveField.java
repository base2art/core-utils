package com.base2art.util.reflect;

public interface IReflectiveField<TParent, TResult>
{
  TResult get();

  IReflectiveClass<TResult> type();
}
