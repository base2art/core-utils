package com.base2art.util.reflect;

public abstract class ReflectiveBase<TParent>
{

  private final ReflectiveClassHolder<TParent> holder;

  protected ReflectiveBase(ReflectiveClassHolder<TParent> holder)
  {
    this.holder = holder;
  }

  public boolean isReturnNullOnError()
  {
    return this.holder.isReturnNullOnError();
  }

  protected final ReflectiveClassHolder<TParent> getHolder()
  {
    return this.holder;
  }

  protected <TItem> TItem invokeGeneric(IReflectiveCallable<TItem> callable)
  {
    try
    {
      return callable.call();
    }
    catch (ReflectiveOperationException ex)
    {
      return this.<TItem>returnOrThrow(ex);
    }
    catch (SecurityException ex)
    {
      return this.<TItem>returnOrThrow(ex);
    }
    catch (IllegalArgumentException ex)
    {
      return this.<TItem>returnOrThrow(ex);
    }
  }

  protected <T> T returnOrThrow(Exception ex)
  {
    if (this.holder.isReturnNullOnError())
    {
      return null;
    }
    throw new RuntimeException("Could Not Invoke", ex);
  }
}
