package com.base2art.util.reflect;

public class Reflective
{

  public static IReflectiveClass<?> forClass(ClassLoader classloader, String className)
  {
    ReflectiveClassHolder holder = new ReflectiveClassHolder(classloader, className, false);
    return new ReflectiveClass(holder);
  }

  public static <T> IReflectiveClass<T> forClass(Class<T> klazz)
  {
    ReflectiveClassHolder<T> holder = new ReflectiveClassHolder<T>(klazz, false);
    return new ReflectiveClass<T>(holder);
  }

  public static IReflectiveClass<?> forClass(ClassLoader classloader, String className, boolean returnNullOnError)
  {
    ReflectiveClassHolder holder = new ReflectiveClassHolder(classloader, className, returnNullOnError);
    return new ReflectiveClass(holder);
  }

  public static <T> IReflectiveClass<T> forClass(Class<T> klazz, boolean returnNullOnError)
  {
    ReflectiveClassHolder holder = new ReflectiveClassHolder(klazz, returnNullOnError);
    return new ReflectiveClass<T>(holder);
  }

  public static <T> IReflectiveClass<T> forClass(ReflectiveClassHolder<?> holder, Class<T> klazz)
  {
    return new ReflectiveClass(new ReflectiveClassHolder<T>(holder, klazz));
  }
}
