package com.base2art.util;

public class SingleSetValueConainer<T>
  implements IValueContainer<T>
{

  private T value;
  private boolean hasSet;
  private final boolean allowSetIfPreviousValueIsNull;

  public SingleSetValueConainer()
  {
    this(false);
  }

  public SingleSetValueConainer(boolean allowSetIfPreviousValueIsNull)
  {
    this.allowSetIfPreviousValueIsNull = allowSetIfPreviousValueIsNull;
  }

  @Override
  public T getValue()
  {
    return this.value;
  }

  @Override
  public void setValue(T value)
  {
    if (this.hasSet)
    {
      return;
    }

    if (this.allowSetIfPreviousValueIsNull && value == null)
    {
      return;
    }

    this.hasSet = true;
    this.value = value;
  }
}
