package com.base2art.util.mapper;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

public class SimpleDataObjectMapper<TFrom, TTo>
  implements IDataMappingAction<TFrom, TTo>
{

  private final Class<TFrom> mainFromKlazz;
  private final Class<TTo> mainToKlazz;
  private final HashMap<Class<?>, Class<?>> childMappings = new HashMap<Class<?>, Class<?>>();
  private final HashSet<String> excudes = new HashSet<String>();

  public SimpleDataObjectMapper(Class<TFrom> fromKlazz, Class<TTo> toKlazz)
  {
    this.mainFromKlazz = fromKlazz;
    this.mainToKlazz = toKlazz;
  }

  @Override
  public void map(TFrom fromInstance, TTo toInstance)
  {
    this.mapUnsafe(fromInstance, this.mainFromKlazz, toInstance, this.mainToKlazz);
  }

  private boolean isMethodGetter(Method method)
  {
    final String name = method.getName();

    if ("getClass".equals(name))
    {
      return false;
    }

    if (name.startsWith("is") || name.startsWith("get"))
    {
      if (name.startsWith("get") && name.length() > 3)
      {
        return !this.excudes.contains(name.substring(3, 4).toLowerCase() + name.substring(4).toLowerCase());
      }

      if (name.startsWith("is") && name.length() > 2)
      {
        return !this.excudes.contains(name.substring(2, 3).toLowerCase() + name.substring(3).toLowerCase());
      }
    }

    return false;
  }

  private boolean isMethodReturnTypePrimitiveOrSimple(Class<?> returnType)
  {
    if (Void.TYPE.equals(returnType))
    {
      return false;
    }

    if (returnType.isPrimitive())
    {
      return true;
    }

    if (String.class.equals(returnType))
    {
      return true;
    }

    if (Number.class.equals(returnType.getSuperclass()))
    {
      return true;
    }

    if (UUID.class.equals(returnType))
    {
      return true;
    }

    return false;
  }

  private boolean isMapable(Class<?> returnType)
  {
    if (Void.TYPE.equals(returnType))
    {
      return false;
    }

    int modifiers = returnType.getModifiers();

    if (Modifier.isStatic(modifiers))
    {
      return false;
    }

    return true;
  }

  private Class<?> mapReturnType(Class<?> returnKlazz)
  {
    Class<?> setterType = returnKlazz;
    if (this.childMappings.containsKey(setterType))
    {
      setterType = this.childMappings.get(setterType);
    }
    return setterType;
  }

  private String translateName(String name)
  {
    if (name.startsWith("get"))
    {
      return "s" + name.substring(1);
    }

    if (name.startsWith("is"))
    {
      return "setI" + name.substring(1);
    }

    return name;
  }

  private boolean isMethodParameterCount(Method method, int length)
  {
    return method.getParameterTypes().length == length;
  }

  public void addMapping(Class<?> fromKlazz, Class<?> toKlazz)
  {
    if (childMappings.containsKey(fromKlazz))
    {
      throw new java.lang.IllegalArgumentException("Duplicate item in collection: '" + fromKlazz + "'");
    }

    this.childMappings.put(fromKlazz, toKlazz);
  }

  private void mapUnsafe(Object fromInstance, Class<?> fromKlazz, Object toInstance, Class<?> toKlazz)
  {
    Method[] methods = fromKlazz.getMethods();
    for (Method getterMethodTemp : methods)
    {
      Method getterMethod = getterMethodTemp;

      if (this.isMethodGetter(getterMethod))
      {
        try
        {
          Class<?> returnKlazz = getterMethod.getReturnType();

          if (this.isMapable(returnKlazz))
          {

            String name = getterMethod.getName();
            String setterName = this.translateName(name);
            if (this.isMethodParameterCount(getterMethod, 0))
            {
              if (returnKlazz.isArray())
              {
                Class<?> componentType = returnKlazz.getComponentType();

                ReflectionTuple tuple = this.getTuple(getterMethod, componentType, fromInstance.getClass());
                componentType = tuple.getReturnKlazz();
                getterMethod = tuple.getGetterMethod();

                Class<?> setterType = mapReturnType(componentType);

                Object newArray = Array.newInstance(setterType, 0);
                Class<?> arraySetterType = newArray.getClass();
                Method setterMethod = toKlazz.getMethod(setterName, arraySetterType);
                this.mapSingleDataObject(setterMethod, returnKlazz, setterType, getterMethod, fromInstance, toInstance);
              }
              else if (this.isMethodReturnTypePrimitiveOrSimple(returnKlazz))
              {
                Class<?> setterType = mapReturnType(returnKlazz);
                Method setterMethod = toKlazz.getMethod(setterName, setterType);
                this.mapSingleDataObject(setterMethod, returnKlazz, setterType, getterMethod, fromInstance, toInstance);
              }
              else
              {
                ReflectionTuple tuple = this.getTuple(getterMethod, returnKlazz, fromInstance.getClass());

                returnKlazz = tuple.getReturnKlazz();
                getterMethod = tuple.getGetterMethod();

                Class<?> setterType = mapReturnType(returnKlazz);

                Method setterMethod = toKlazz.getMethod(setterName, setterType);
                this.mapSingleDataObject(setterMethod, returnKlazz, setterType, getterMethod, fromInstance, toInstance);
              }
            }
          }
        }
        catch (ReflectiveOperationException ex)
        {
          throw new RuntimeException(
            fromKlazz.toString()
            + " : "
            + getterMethod.getReturnType().toString(),
            ex);
        }
      }
    }
  }

  private ReflectionTuple getTuple(
    Method getterMethod,
    Class<?> returnKlazz,
    Class<?> fromInstanceKlazz)
  {
    try
    {
      if (returnKlazz == Object.class && getterMethod.getGenericReturnType() != null)
      {
        Class<?> concreteKlass = fromInstanceKlazz;
        getterMethod = concreteKlass.getDeclaredMethod(
          getterMethod.getName(),
          getterMethod.getParameterTypes());
        returnKlazz = getterMethod.getReturnType();
        if (returnKlazz.isArray())
        {
          returnKlazz = returnKlazz.getComponentType();
        }

        return new ReflectionTuple(getterMethod, returnKlazz);
      }

      return new ReflectionTuple(getterMethod, returnKlazz);
    }
    catch (NoSuchMethodException e)
    {
      final Class<?> superclass = fromInstanceKlazz.getSuperclass();
      if (superclass != null)
      {
        return this.getTuple(getterMethod, returnKlazz, superclass);
      }

      return new ReflectionTuple(getterMethod, returnKlazz);
    }
  }

  private void mapSingleDataObject(
    Method setterMethod, Class<?> returnKlazz, Class<?> setterType, Method getterMethod, Object fromInstance, Object toInstance)
    throws InvocationTargetException, IllegalAccessException, InstantiationException, IllegalArgumentException
  {
    if (setterMethod != null && this.isMethodParameterCount(setterMethod, 1))
    {
      if (returnKlazz.equals(setterType))
      {
        final Object getValue = getterMethod.invoke(fromInstance, (Object[]) null);
        setterMethod.invoke(toInstance, getValue);
      }
      else if (returnKlazz.isArray())
      {
        final Object childFromInstance = getterMethod.invoke(fromInstance, (Object[]) null);
        if (childFromInstance != null)
        {
          final int length = Array.getLength(childFromInstance);
          Object childToInstance = Array.newInstance(setterType, length);
          setterMethod.invoke(toInstance, childToInstance);
          boolean isSimple = this.isMethodReturnTypePrimitiveOrSimple(setterType);

          for (int i = 0; i < length; i++)
          {
            if (isSimple)
            {
              Array.set(childToInstance, i, Array.get(childFromInstance, i));
            }
            else
            {
              Object setterInstance = setterType.newInstance();
              this.mapUnsafe(
                Array.get(childFromInstance, i), returnKlazz.getComponentType(),
                setterInstance, setterType);

              Array.set(childToInstance, i, setterInstance);
            }
          }
        }
      }
      else
      {
        final Object childFromInstance = getterMethod.invoke(fromInstance, (Object[]) null);
        if (childFromInstance != null)
        {
          Object childToInstance = setterType.newInstance();
          setterMethod.invoke(toInstance, childToInstance);
          this.mapUnsafe(childFromInstance, returnKlazz, childToInstance, setterType);
        }
      }
    }
  }

  public void exclude(String name)
  {
    name = name.toLowerCase();
    if (name.startsWith("get"))
    {
      name = name.substring(3);
    }
    else if (name.startsWith("is"))
    {
      name = name.substring(2);
    }

    this.excudes.add(name);
  }
}
