package com.base2art.util.mapper;

import java.lang.reflect.Method;

class ReflectionTuple
{

  private Method getterMethod;
  private Class<?> returnKlazz;

  ReflectionTuple()
  {
  }

  ReflectionTuple(Method getterMethod, Class<?> returnKlazz)
  {
    this.getterMethod = getterMethod;
    this.returnKlazz = returnKlazz;
  }

  public Method getGetterMethod()
  {
    return getterMethod;
  }

  public void setGetterMethod(Method getterMethod)
  {
    this.getterMethod = getterMethod;
  }

  public Class<?> getReturnKlazz()
  {
    return returnKlazz;
  }

  public void setReturnKlazz(Class<?> returnKlazz)
  {
    this.returnKlazz = returnKlazz;
  }


}
