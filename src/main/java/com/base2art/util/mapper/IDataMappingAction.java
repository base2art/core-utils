package com.base2art.util.mapper;

public interface IDataMappingAction<TSource, TDestination>
{

  void map(TSource source, TDestination destination);
}
