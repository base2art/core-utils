

mapping = [
            ("Char",    "char",    "Character", "",        ['(char)55', '(char)56', '(char)57', '(char)58', '(char)59']),
            ("Byte",    "byte",    "Byte" ,     "",        ['(byte)55', '(byte)56', '(byte)57', '(byte)58', '(byte)59']),
            ("Short",   "short",   "Short",     "",        ['(short)55', '(short)56', '(short)57', '(short)58', '(short)59']),
            ("Int",     "int",     "Integer",   "",        ['55', '56', '57', '58', '59']),
            ("Long",    "long",    "Long",      "",        ['55', '56', '57', '58', '59']),
            ("Float",   "float",   "Float",     "",        ['55', '56', '57', '58', '59']),
            ("Double",  "double",  "Double",    "",        ['55', '56', '57', '58', '59']),
            ("Boolean", "boolean", "Boolean",   "@Ignore", ['true', 'true', 'true', 'true', 'false']),
]



def writeArrayConverter(class_name, formats):
  template_fh = open("PrimitiveArrayConverterTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/collections/converters/%sArrayConverter.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeComparatorInterface(class_name, formats):
  template_fh = open("PrimitiveComparatorImplementationTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/comparators/%sComparator.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeRAROCInterface(class_name, formats):
  template_fh = open("PrimitiveCollectionRAROCInterfaceTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/collections/specialized/I%sRandomAccessReadonlyCollection.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeSearchableInterface(class_name, formats):
  template_fh = open("PrimitiveCollectionSearchableInterfaceTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/collections/specialized/I%sSearchable.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeListInterface(class_name, formats):
  template_fh = open("PrimitiveCollectionListInterfaceTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/collections/specialized/I%sList.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeSearchableListInterface(class_name, formats):
  template_fh = open("PrimitiveCollectionSearchableListInterfaceTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/collections/specialized/I%sSearchableList.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeImplementation(class_name, formats):
  template_fh = open("PrimitiveCollectionListImplementationTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../java/com/base2art/collections/specialized/Generic%sList.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

def writeTests(class_name, formats):
  template_fh = open("PrimitiveCollectionTestTemplate.jt")
  template = template_fh.read()
  template_fh.close()
  fh = open("../../test/java/com/base2art/collections/specialized/%sListTest.java" % (class_name), "w+")
  fh.write(template % formats)
  fh.close()

for class_name, primitive_name, boxed_name, ignore_test_attr, samples in mapping:
  formats = {
        "class_name"      : class_name, 
        "primitive_name"  : primitive_name,
        "boxed_name"      : boxed_name,
        "index_test_attr" : ignore_test_attr,
        "sample_1"        : samples[0],
        "sample_2"        : samples[1],
        "sample_3"        : samples[2],
        "sample_4"        : samples[3],
        "sample_5"        : samples[4]
  }
  writeComparatorInterface(boxed_name, formats)
  writeArrayConverter(boxed_name, formats)

  writeRAROCInterface(class_name, formats)
  writeSearchableInterface(class_name, formats)
  writeListInterface(class_name, formats)
  writeSearchableListInterface(class_name, formats)
  writeImplementation(class_name, formats)
  writeTests(class_name, formats)

