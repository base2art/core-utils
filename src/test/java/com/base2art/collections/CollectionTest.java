package com.base2art.collections;

import com.base2art.lang.StringUtil;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class CollectionTest
{

  @Test
  public void shouldSomething()
  {
    IStringCollection strColl = StringUtil.split("1-2-3-4-", '-');
    assertThat(strColl.size()).isEqualTo(4);
    StringCollection strCollE = strColl.asList();
    strCollE.add("5");
    assertThat(strColl.size()).isEqualTo(4);
    assertThat(strCollE.size()).isEqualTo(5);
//    assertThat().
  }
}