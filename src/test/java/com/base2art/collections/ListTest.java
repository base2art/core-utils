package com.base2art.collections;

import com.base2art.comparators.StringComparator;
import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import org.junit.Test;

public class ListTest
{

  @Test
  public void shouldAdd()
  {
    IList<String> strColl = new GenericList<String>(String.class);
    assertThat(strColl.size()).isEqualTo(0);
    IList<String> strCollE = strColl.asList();
    strCollE.add("5");
    assertThat(strColl.size()).isEqualTo(0);
    assertThat(strCollE.size()).isEqualTo(1);
  }

  @Test
  public void shouldSetCorrectly()
  {
    IList<String> strColl = new GenericList<String>(String.class);
    strColl.add("5");
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo("5");
    strColl.set(0, "4");
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo("4");
  }

  @Test
  public void shouldSetIndexOutofBounds()
  {
    IList<String> strColl = new GenericList<String>(String.class);
    strColl.add("5");
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo("5");

    try
    {
      strColl.set(1, "4");
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
      assertThat(e).hasMessage("Index: 1, Size: 1");
    }
  }

  @Test
  public void shouldClear()
  {
    IList<String> strColl = new GenericList<String>(String.class);
    strColl.add("5");
    strColl.add("4");
    assertThat(strColl.size()).isEqualTo(2);
    strColl.clear();
    assertThat(strColl.size()).isEqualTo(0);
  }

  @Test
  public void shouldAddAtIndex()
  {
    IList<String> strColl = new GenericList<String>(String.class);
    strColl.add("5");
    strColl.addAt(0, "4");
    assertThat(strColl.size()).isEqualTo(2);
    assertThat(strColl.get(0)).isEqualTo("4");
    assertThat(strColl.get(1)).isEqualTo("5");
  }

  @Test
  public void shouldRemoveAt()
  {
    GenericList<String> strColl = new GenericList<String>(String.class, new StringComparator());
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");

    IList<String> list = strColl;

    // Hack For String Interning blah blah blah
    assertThat(list.removeAt(1)).isEqualTo("2");
    assertThat(list.get(0)).isEqualTo("1");
    assertThat(list.get(1)).isEqualTo("3");
    assertThat(list.get(2)).isEqualTo("4");
    assertThat(list.get(3)).isEqualTo("5");
    
    try
    {
      list.removeAt(5);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
    }
  }

  @Test
  public void shouldSearch()
  {
    GenericList<String> strColl = new GenericList<String>(String.class, new StringComparator());
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");

    ISearchable<String> searchable = strColl;

    // Hack For String Interning blah blah blah
    assertThat(searchable.contains(toString(51))).isTrue();
    assertThat(searchable.contains(toString(55))).isFalse();
    assertThat(searchable.contains("3")).isTrue();
  }

  @Test
  public void shouldIndexOf()
  {
    GenericList<String> strColl = new GenericList<String>(String.class, new StringComparator());
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");

    ISearchableList<String> searchable = strColl;

    // Hack For String Interning blah blah blah
    assertThat(searchable.indexOf(toString(51))).isEqualTo(2);
    assertThat(searchable.indexOf(toString(52))).isEqualTo(3);
    assertThat(searchable.indexOf(toString(53))).isEqualTo(4);
    assertThat(searchable.indexOf("3")).isEqualTo(2);
    try
    {
      searchable.indexOf(toString(55));
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf(toString(51), false)).isEqualTo(2);
    assertThat(searchable.indexOf(toString(52), false)).isEqualTo(3);
    assertThat(searchable.indexOf(toString(53), false)).isEqualTo(4);
    assertThat(searchable.indexOf("3", false)).isEqualTo(2);
    try
    {
      searchable.indexOf(toString(55), false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf(toString(51), true)).isEqualTo(2);
    assertThat(searchable.indexOf(toString(52), true)).isEqualTo(3);
    assertThat(searchable.indexOf(toString(53), true)).isEqualTo(4);
    assertThat(searchable.indexOf("3", true)).isEqualTo(2);
    assertThat(searchable.indexOf(toString(55), true)).isEqualTo(-1);
  }

  @Test
  public void shouldLastIndexOf()
  {
    GenericList<String> strColl = new GenericList<String>(String.class, new StringComparator());
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");

    ISearchableList<String> searchable = strColl;

    // Hack For String Interning blah blah blah
    assertThat(searchable.lastIndexOf(toString(51))).isEqualTo(2 + 5);
    assertThat(searchable.lastIndexOf(toString(52))).isEqualTo(3 + 5);
    assertThat(searchable.lastIndexOf(toString(53))).isEqualTo(4 + 5);
    assertThat(searchable.lastIndexOf("3")).isEqualTo(2 + 5);
    try
    {
      searchable.lastIndexOf(toString(55));
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf(toString(51), false)).isEqualTo(2 + 5);
    assertThat(searchable.lastIndexOf(toString(52), false)).isEqualTo(3 + 5);
    assertThat(searchable.lastIndexOf(toString(53), false)).isEqualTo(4 + 5);
    assertThat(searchable.lastIndexOf("3", false)).isEqualTo(2 + 5);
    try
    {
      searchable.lastIndexOf(toString(55), false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf(toString(51), true)).isEqualTo(2 + 5);
    assertThat(searchable.lastIndexOf(toString(52), true)).isEqualTo(3 + 5);
    assertThat(searchable.lastIndexOf(toString(53), true)).isEqualTo(4 + 5);
    assertThat(searchable.lastIndexOf("3", true)).isEqualTo(2 + 5);
    assertThat(searchable.lastIndexOf(toString(55), true)).isEqualTo(-1);
  }

  @Test
  public void shouldRemove()
  {
    GenericList<String> strColl = new GenericList<String>(String.class, new StringComparator());
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");

    ISearchableList<String> searchable = strColl;
    final int[] removedOccurances = searchable.remove(toString(51));

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(2);
    assertThat(removedOccurances[0]).isEqualTo(2);
    assertThat(removedOccurances[1]).isEqualTo(2 + 5);
  }

  @Test
  public void shouldToArray()
  {
    GenericList<String> strColl = new GenericList<String>(String.class, new StringComparator());
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");
    strColl.add("1");
    strColl.add("2");
    strColl.add("3");
    strColl.add("4");
    strColl.add("5");

    IList<String> searchable = strColl;
    final String[] removedOccurances = searchable.toArray();

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(searchable.size());
    assertThat(removedOccurances[0]).isEqualTo(searchable.get(0));
    assertThat(removedOccurances[1]).isEqualTo(searchable.get(1));
    assertThat(removedOccurances[2]).isEqualTo(searchable.get(2));
    assertThat(removedOccurances[3]).isEqualTo(searchable.get(3));
    assertThat(removedOccurances[4]).isEqualTo(searchable.get(4));
    assertThat(removedOccurances[5]).isEqualTo(searchable.get(5));
    assertThat(removedOccurances[6]).isEqualTo(searchable.get(6));
    assertThat(removedOccurances[7]).isEqualTo(searchable.get(7));
    assertThat(removedOccurances[8]).isEqualTo(searchable.get(8));
    assertThat(removedOccurances[9]).isEqualTo(searchable.get(9));
  }

  private String toString(int input)
  {
    return new String(new char[]
    {
      (char) input
    });
  }
}
