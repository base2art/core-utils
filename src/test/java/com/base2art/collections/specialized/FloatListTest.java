package com.base2art.collections.specialized;

import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;


public class FloatListTest
{

  @Test
  public void shouldAdd()
  {
    IFloatList strColl = new GenericFloatList();
    assertThat(strColl.size()).isEqualTo(0);
    IFloatList strCollE = strColl.asList();
    strCollE.add(55);
    assertThat(strColl.size()).isEqualTo(0);
    assertThat(strCollE.size()).isEqualTo(1);
  }

  @Test
  public void shouldSetCorrectly()
  {
    IFloatList strColl = new GenericFloatList();
    strColl.add(55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo(55);
    strColl.set(0, 56);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo(56);
  }

  @Test
  public void shouldSetIndexOutofBounds()
  {
    IFloatList strColl = new GenericFloatList();
    strColl.add(55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo(55);

    try
    {
      strColl.set(1, 56);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
      assertThat(e).hasMessage("Index: 1, Size: 1");
    }
  }

  @Test
  public void shouldClear()
  {
    IFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    assertThat(strColl.size()).isEqualTo(2);
    strColl.clear();
    assertThat(strColl.size()).isEqualTo(0);
  }

  @Test
  public void shouldAddAtIndex()
  {
    IFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.addAt(0, 56);
    assertThat(strColl.size()).isEqualTo(2);
    assertThat(strColl.get(0)).isEqualTo(56);
    assertThat(strColl.get(1)).isEqualTo(55);
  }

  @Test
  public void shouldRemoveAt()
  {
    GenericFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);

    IFloatList list = strColl;

    // Hack For String Interning blah blah blah
    assertThat(list.removeAt(1)).isEqualTo(56);
    assertThat(list.get(0)).isEqualTo(55);
    assertThat(list.get(1)).isEqualTo(57);
    assertThat(list.get(2)).isEqualTo(58);
    
    try
    {
      list.removeAt(5);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
    }
  }

  @Test
  public void shouldSearch()
  {
    GenericFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);

    IFloatSearchable searchable = strColl;

    assertThat(searchable.contains(55)).isTrue();
    assertThat(searchable.contains(58)).isTrue();
    assertThat(searchable.contains(59)).isFalse();
  }

  
  @Test
  public void shouldIndexOf()
  {
    GenericFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);

    IFloatSearchableList searchable = strColl;

    assertThat(searchable.indexOf(55)).isEqualTo(0);
    assertThat(searchable.indexOf(56)).isEqualTo(1);
    assertThat(searchable.indexOf(57)).isEqualTo(2);
    try
    {
      searchable.indexOf(59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf(55, false)).isEqualTo(0);
    assertThat(searchable.indexOf(56, false)).isEqualTo(1);
    assertThat(searchable.indexOf(57, false)).isEqualTo(2);
    try
    {
      searchable.indexOf(59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf(55, true)).isEqualTo(0);
    assertThat(searchable.indexOf(56, true)).isEqualTo(1);
    assertThat(searchable.indexOf(57, true)).isEqualTo(2);
    assertThat(searchable.indexOf(59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldLastIndexOf()
  {
    GenericFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);

    IFloatSearchableList searchable = strColl;

    assertThat(searchable.lastIndexOf(55)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf(56)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf(57)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf(59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf(55, false)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf(56, false)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf(57, false)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf(59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf(55, true)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf(56, true)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf(57, true)).isEqualTo(2 + 4);
    assertThat(searchable.lastIndexOf(59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldRemove()
  {
    GenericFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);

    IFloatSearchableList searchable = strColl;
    final int[] removedOccurances = searchable.remove(57);

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(2);
    assertThat(removedOccurances[0]).isEqualTo(2);
    assertThat(removedOccurances[1]).isEqualTo(2 + 4);
  }

  @Test
  public void shouldToArray()
  {
    GenericFloatList strColl = new GenericFloatList();
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);
    strColl.add(55);
    strColl.add(56);
    strColl.add(57);
    strColl.add(58);

    IFloatList searchable = strColl;
    final float[] removedOccurances = searchable.toArray();

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(searchable.size());
    assertThat(removedOccurances[0]).isEqualTo(searchable.get(0));
    assertThat(removedOccurances[1]).isEqualTo(searchable.get(1));
    assertThat(removedOccurances[2]).isEqualTo(searchable.get(2));
    assertThat(removedOccurances[3]).isEqualTo(searchable.get(3));
    assertThat(removedOccurances[4]).isEqualTo(searchable.get(4));
    assertThat(removedOccurances[5]).isEqualTo(searchable.get(5));
    assertThat(removedOccurances[6]).isEqualTo(searchable.get(6));
    assertThat(removedOccurances[7]).isEqualTo(searchable.get(7));
  }
}
