package com.base2art.collections.specialized;

import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;


public class CharListTest
{

  @Test
  public void shouldAdd()
  {
    ICharList strColl = new GenericCharList();
    assertThat(strColl.size()).isEqualTo(0);
    ICharList strCollE = strColl.asList();
    strCollE.add((char)55);
    assertThat(strColl.size()).isEqualTo(0);
    assertThat(strCollE.size()).isEqualTo(1);
  }

  @Test
  public void shouldSetCorrectly()
  {
    ICharList strColl = new GenericCharList();
    strColl.add((char)55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((char)55);
    strColl.set(0, (char)56);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((char)56);
  }

  @Test
  public void shouldSetIndexOutofBounds()
  {
    ICharList strColl = new GenericCharList();
    strColl.add((char)55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((char)55);

    try
    {
      strColl.set(1, (char)56);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
      assertThat(e).hasMessage("Index: 1, Size: 1");
    }
  }

  @Test
  public void shouldClear()
  {
    ICharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    assertThat(strColl.size()).isEqualTo(2);
    strColl.clear();
    assertThat(strColl.size()).isEqualTo(0);
  }

  @Test
  public void shouldAddAtIndex()
  {
    ICharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.addAt(0, (char)56);
    assertThat(strColl.size()).isEqualTo(2);
    assertThat(strColl.get(0)).isEqualTo((char)56);
    assertThat(strColl.get(1)).isEqualTo((char)55);
  }

  @Test
  public void shouldRemoveAt()
  {
    GenericCharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);

    ICharList list = strColl;

    // Hack For String Interning blah blah blah
    assertThat(list.removeAt(1)).isEqualTo((char)56);
    assertThat(list.get(0)).isEqualTo((char)55);
    assertThat(list.get(1)).isEqualTo((char)57);
    assertThat(list.get(2)).isEqualTo((char)58);
    
    try
    {
      list.removeAt(5);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
    }
  }

  @Test
  public void shouldSearch()
  {
    GenericCharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);

    ICharSearchable searchable = strColl;

    assertThat(searchable.contains((char)55)).isTrue();
    assertThat(searchable.contains((char)58)).isTrue();
    assertThat(searchable.contains((char)59)).isFalse();
  }

  
  @Test
  public void shouldIndexOf()
  {
    GenericCharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);

    ICharSearchableList searchable = strColl;

    assertThat(searchable.indexOf((char)55)).isEqualTo(0);
    assertThat(searchable.indexOf((char)56)).isEqualTo(1);
    assertThat(searchable.indexOf((char)57)).isEqualTo(2);
    try
    {
      searchable.indexOf((char)59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf((char)55, false)).isEqualTo(0);
    assertThat(searchable.indexOf((char)56, false)).isEqualTo(1);
    assertThat(searchable.indexOf((char)57, false)).isEqualTo(2);
    try
    {
      searchable.indexOf((char)59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf((char)55, true)).isEqualTo(0);
    assertThat(searchable.indexOf((char)56, true)).isEqualTo(1);
    assertThat(searchable.indexOf((char)57, true)).isEqualTo(2);
    assertThat(searchable.indexOf((char)59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldLastIndexOf()
  {
    GenericCharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);

    ICharSearchableList searchable = strColl;

    assertThat(searchable.lastIndexOf((char)55)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((char)56)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((char)57)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf((char)59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf((char)55, false)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((char)56, false)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((char)57, false)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf((char)59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf((char)55, true)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((char)56, true)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((char)57, true)).isEqualTo(2 + 4);
    assertThat(searchable.lastIndexOf((char)59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldRemove()
  {
    GenericCharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);

    ICharSearchableList searchable = strColl;
    final int[] removedOccurances = searchable.remove((char)57);

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(2);
    assertThat(removedOccurances[0]).isEqualTo(2);
    assertThat(removedOccurances[1]).isEqualTo(2 + 4);
  }

  @Test
  public void shouldToArray()
  {
    GenericCharList strColl = new GenericCharList();
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);
    strColl.add((char)55);
    strColl.add((char)56);
    strColl.add((char)57);
    strColl.add((char)58);

    ICharList searchable = strColl;
    final char[] removedOccurances = searchable.toArray();

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(searchable.size());
    assertThat(removedOccurances[0]).isEqualTo(searchable.get(0));
    assertThat(removedOccurances[1]).isEqualTo(searchable.get(1));
    assertThat(removedOccurances[2]).isEqualTo(searchable.get(2));
    assertThat(removedOccurances[3]).isEqualTo(searchable.get(3));
    assertThat(removedOccurances[4]).isEqualTo(searchable.get(4));
    assertThat(removedOccurances[5]).isEqualTo(searchable.get(5));
    assertThat(removedOccurances[6]).isEqualTo(searchable.get(6));
    assertThat(removedOccurances[7]).isEqualTo(searchable.get(7));
  }
}
