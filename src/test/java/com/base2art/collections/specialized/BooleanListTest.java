package com.base2art.collections.specialized;

import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;


public class BooleanListTest
{

  @Test
  public void shouldAdd()
  {
    IBooleanList strColl = new GenericBooleanList();
    assertThat(strColl.size()).isEqualTo(0);
    IBooleanList strCollE = strColl.asList();
    strCollE.add(true);
    assertThat(strColl.size()).isEqualTo(0);
    assertThat(strCollE.size()).isEqualTo(1);
  }

  @Test
  public void shouldSetCorrectly()
  {
    IBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo(true);
    strColl.set(0, true);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo(true);
  }

  @Test
  public void shouldSetIndexOutofBounds()
  {
    IBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo(true);

    try
    {
      strColl.set(1, true);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
      assertThat(e).hasMessage("Index: 1, Size: 1");
    }
  }

  @Test
  public void shouldClear()
  {
    IBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    assertThat(strColl.size()).isEqualTo(2);
    strColl.clear();
    assertThat(strColl.size()).isEqualTo(0);
  }

  @Test
  public void shouldAddAtIndex()
  {
    IBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.addAt(0, true);
    assertThat(strColl.size()).isEqualTo(2);
    assertThat(strColl.get(0)).isEqualTo(true);
    assertThat(strColl.get(1)).isEqualTo(true);
  }

  @Test
  public void shouldRemoveAt()
  {
    GenericBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);

    IBooleanList list = strColl;

    // Hack For String Interning blah blah blah
    assertThat(list.removeAt(1)).isEqualTo(true);
    assertThat(list.get(0)).isEqualTo(true);
    assertThat(list.get(1)).isEqualTo(true);
    assertThat(list.get(2)).isEqualTo(true);
    
    try
    {
      list.removeAt(5);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
    }
  }

  @Test
  public void shouldSearch()
  {
    GenericBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);

    IBooleanSearchable searchable = strColl;

    assertThat(searchable.contains(true)).isTrue();
    assertThat(searchable.contains(true)).isTrue();
    assertThat(searchable.contains(false)).isFalse();
  }

  @Ignore
  @Test
  public void shouldIndexOf()
  {
    GenericBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);

    IBooleanSearchableList searchable = strColl;

    assertThat(searchable.indexOf(true)).isEqualTo(0);
    assertThat(searchable.indexOf(true)).isEqualTo(1);
    assertThat(searchable.indexOf(true)).isEqualTo(2);
    try
    {
      searchable.indexOf(false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf(true, false)).isEqualTo(0);
    assertThat(searchable.indexOf(true, false)).isEqualTo(1);
    assertThat(searchable.indexOf(true, false)).isEqualTo(2);
    try
    {
      searchable.indexOf(false, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf(true, true)).isEqualTo(0);
    assertThat(searchable.indexOf(true, true)).isEqualTo(1);
    assertThat(searchable.indexOf(true, true)).isEqualTo(2);
    assertThat(searchable.indexOf(false, true)).isEqualTo(-1);
  }

  @Ignore
  @Test
  public void shouldLastIndexOf()
  {
    GenericBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);

    IBooleanSearchableList searchable = strColl;

    assertThat(searchable.lastIndexOf(true)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf(true)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf(true)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf(false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf(true, false)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf(true, false)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf(true, false)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf(false, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf(true, true)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf(true, true)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf(true, true)).isEqualTo(2 + 4);
    assertThat(searchable.lastIndexOf(false, true)).isEqualTo(-1);
  }

  @Ignore
  @Test
  public void shouldRemove()
  {
    GenericBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);

    IBooleanSearchableList searchable = strColl;
    final int[] removedOccurances = searchable.remove(true);

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(2);
    assertThat(removedOccurances[0]).isEqualTo(2);
    assertThat(removedOccurances[1]).isEqualTo(2 + 4);
  }

  @Test
  public void shouldToArray()
  {
    GenericBooleanList strColl = new GenericBooleanList();
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);
    strColl.add(true);

    IBooleanList searchable = strColl;
    final boolean[] removedOccurances = searchable.toArray();

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(searchable.size());
    assertThat(removedOccurances[0]).isEqualTo(searchable.get(0));
    assertThat(removedOccurances[1]).isEqualTo(searchable.get(1));
    assertThat(removedOccurances[2]).isEqualTo(searchable.get(2));
    assertThat(removedOccurances[3]).isEqualTo(searchable.get(3));
    assertThat(removedOccurances[4]).isEqualTo(searchable.get(4));
    assertThat(removedOccurances[5]).isEqualTo(searchable.get(5));
    assertThat(removedOccurances[6]).isEqualTo(searchable.get(6));
    assertThat(removedOccurances[7]).isEqualTo(searchable.get(7));
  }
}
