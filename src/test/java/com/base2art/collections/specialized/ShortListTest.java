package com.base2art.collections.specialized;

import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;


public class ShortListTest
{

  @Test
  public void shouldAdd()
  {
    IShortList strColl = new GenericShortList();
    assertThat(strColl.size()).isEqualTo(0);
    IShortList strCollE = strColl.asList();
    strCollE.add((short)55);
    assertThat(strColl.size()).isEqualTo(0);
    assertThat(strCollE.size()).isEqualTo(1);
  }

  @Test
  public void shouldSetCorrectly()
  {
    IShortList strColl = new GenericShortList();
    strColl.add((short)55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((short)55);
    strColl.set(0, (short)56);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((short)56);
  }

  @Test
  public void shouldSetIndexOutofBounds()
  {
    IShortList strColl = new GenericShortList();
    strColl.add((short)55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((short)55);

    try
    {
      strColl.set(1, (short)56);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
      assertThat(e).hasMessage("Index: 1, Size: 1");
    }
  }

  @Test
  public void shouldClear()
  {
    IShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    assertThat(strColl.size()).isEqualTo(2);
    strColl.clear();
    assertThat(strColl.size()).isEqualTo(0);
  }

  @Test
  public void shouldAddAtIndex()
  {
    IShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.addAt(0, (short)56);
    assertThat(strColl.size()).isEqualTo(2);
    assertThat(strColl.get(0)).isEqualTo((short)56);
    assertThat(strColl.get(1)).isEqualTo((short)55);
  }

  @Test
  public void shouldRemoveAt()
  {
    GenericShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);

    IShortList list = strColl;

    // Hack For String Interning blah blah blah
    assertThat(list.removeAt(1)).isEqualTo((short)56);
    assertThat(list.get(0)).isEqualTo((short)55);
    assertThat(list.get(1)).isEqualTo((short)57);
    assertThat(list.get(2)).isEqualTo((short)58);
    
    try
    {
      list.removeAt(5);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
    }
  }

  @Test
  public void shouldSearch()
  {
    GenericShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);

    IShortSearchable searchable = strColl;

    assertThat(searchable.contains((short)55)).isTrue();
    assertThat(searchable.contains((short)58)).isTrue();
    assertThat(searchable.contains((short)59)).isFalse();
  }

  
  @Test
  public void shouldIndexOf()
  {
    GenericShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);

    IShortSearchableList searchable = strColl;

    assertThat(searchable.indexOf((short)55)).isEqualTo(0);
    assertThat(searchable.indexOf((short)56)).isEqualTo(1);
    assertThat(searchable.indexOf((short)57)).isEqualTo(2);
    try
    {
      searchable.indexOf((short)59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf((short)55, false)).isEqualTo(0);
    assertThat(searchable.indexOf((short)56, false)).isEqualTo(1);
    assertThat(searchable.indexOf((short)57, false)).isEqualTo(2);
    try
    {
      searchable.indexOf((short)59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf((short)55, true)).isEqualTo(0);
    assertThat(searchable.indexOf((short)56, true)).isEqualTo(1);
    assertThat(searchable.indexOf((short)57, true)).isEqualTo(2);
    assertThat(searchable.indexOf((short)59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldLastIndexOf()
  {
    GenericShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);

    IShortSearchableList searchable = strColl;

    assertThat(searchable.lastIndexOf((short)55)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((short)56)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((short)57)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf((short)59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf((short)55, false)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((short)56, false)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((short)57, false)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf((short)59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf((short)55, true)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((short)56, true)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((short)57, true)).isEqualTo(2 + 4);
    assertThat(searchable.lastIndexOf((short)59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldRemove()
  {
    GenericShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);

    IShortSearchableList searchable = strColl;
    final int[] removedOccurances = searchable.remove((short)57);

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(2);
    assertThat(removedOccurances[0]).isEqualTo(2);
    assertThat(removedOccurances[1]).isEqualTo(2 + 4);
  }

  @Test
  public void shouldToArray()
  {
    GenericShortList strColl = new GenericShortList();
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);
    strColl.add((short)55);
    strColl.add((short)56);
    strColl.add((short)57);
    strColl.add((short)58);

    IShortList searchable = strColl;
    final short[] removedOccurances = searchable.toArray();

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(searchable.size());
    assertThat(removedOccurances[0]).isEqualTo(searchable.get(0));
    assertThat(removedOccurances[1]).isEqualTo(searchable.get(1));
    assertThat(removedOccurances[2]).isEqualTo(searchable.get(2));
    assertThat(removedOccurances[3]).isEqualTo(searchable.get(3));
    assertThat(removedOccurances[4]).isEqualTo(searchable.get(4));
    assertThat(removedOccurances[5]).isEqualTo(searchable.get(5));
    assertThat(removedOccurances[6]).isEqualTo(searchable.get(6));
    assertThat(removedOccurances[7]).isEqualTo(searchable.get(7));
  }
}
