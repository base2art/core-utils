package com.base2art.collections.specialized;

import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.Test;


public class ByteListTest
{

  @Test
  public void shouldAdd()
  {
    IByteList strColl = new GenericByteList();
    assertThat(strColl.size()).isEqualTo(0);
    IByteList strCollE = strColl.asList();
    strCollE.add((byte)55);
    assertThat(strColl.size()).isEqualTo(0);
    assertThat(strCollE.size()).isEqualTo(1);
  }

  @Test
  public void shouldSetCorrectly()
  {
    IByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((byte)55);
    strColl.set(0, (byte)56);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((byte)56);
  }

  @Test
  public void shouldSetIndexOutofBounds()
  {
    IByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    assertThat(strColl.size()).isEqualTo(1);
    assertThat(strColl.get(0)).isEqualTo((byte)55);

    try
    {
      strColl.set(1, (byte)56);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
      assertThat(e).hasMessage("Index: 1, Size: 1");
    }
  }

  @Test
  public void shouldClear()
  {
    IByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    assertThat(strColl.size()).isEqualTo(2);
    strColl.clear();
    assertThat(strColl.size()).isEqualTo(0);
  }

  @Test
  public void shouldAddAtIndex()
  {
    IByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.addAt(0, (byte)56);
    assertThat(strColl.size()).isEqualTo(2);
    assertThat(strColl.get(0)).isEqualTo((byte)56);
    assertThat(strColl.get(1)).isEqualTo((byte)55);
  }

  @Test
  public void shouldRemoveAt()
  {
    GenericByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);

    IByteList list = strColl;

    // Hack For String Interning blah blah blah
    assertThat(list.removeAt(1)).isEqualTo((byte)56);
    assertThat(list.get(0)).isEqualTo((byte)55);
    assertThat(list.get(1)).isEqualTo((byte)57);
    assertThat(list.get(2)).isEqualTo((byte)58);
    
    try
    {
      list.removeAt(5);
      fail("IndexOutOfBoundsException expected");
    }
    catch (IndexOutOfBoundsException e)
    {
    }
  }

  @Test
  public void shouldSearch()
  {
    GenericByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);

    IByteSearchable searchable = strColl;

    assertThat(searchable.contains((byte)55)).isTrue();
    assertThat(searchable.contains((byte)58)).isTrue();
    assertThat(searchable.contains((byte)59)).isFalse();
  }

  
  @Test
  public void shouldIndexOf()
  {
    GenericByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);

    IByteSearchableList searchable = strColl;

    assertThat(searchable.indexOf((byte)55)).isEqualTo(0);
    assertThat(searchable.indexOf((byte)56)).isEqualTo(1);
    assertThat(searchable.indexOf((byte)57)).isEqualTo(2);
    try
    {
      searchable.indexOf((byte)59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf((byte)55, false)).isEqualTo(0);
    assertThat(searchable.indexOf((byte)56, false)).isEqualTo(1);
    assertThat(searchable.indexOf((byte)57, false)).isEqualTo(2);
    try
    {
      searchable.indexOf((byte)59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.indexOf((byte)55, true)).isEqualTo(0);
    assertThat(searchable.indexOf((byte)56, true)).isEqualTo(1);
    assertThat(searchable.indexOf((byte)57, true)).isEqualTo(2);
    assertThat(searchable.indexOf((byte)59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldLastIndexOf()
  {
    GenericByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);

    IByteSearchableList searchable = strColl;

    assertThat(searchable.lastIndexOf((byte)55)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((byte)56)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((byte)57)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf((byte)59);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf((byte)55, false)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((byte)56, false)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((byte)57, false)).isEqualTo(2 + 4);
    try
    {
      searchable.lastIndexOf((byte)59, false);
      fail("IndexOutOfBoundsException expected");
    }
    catch (NoSuchElementException e)
    {
    }

    assertThat(searchable.lastIndexOf((byte)55, true)).isEqualTo(0 + 4);
    assertThat(searchable.lastIndexOf((byte)56, true)).isEqualTo(1 + 4);
    assertThat(searchable.lastIndexOf((byte)57, true)).isEqualTo(2 + 4);
    assertThat(searchable.lastIndexOf((byte)59, true)).isEqualTo(-1);
  }

  
  @Test
  public void shouldRemove()
  {
    GenericByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);

    IByteSearchableList searchable = strColl;
    final int[] removedOccurances = searchable.remove((byte)57);

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(2);
    assertThat(removedOccurances[0]).isEqualTo(2);
    assertThat(removedOccurances[1]).isEqualTo(2 + 4);
  }

  @Test
  public void shouldToArray()
  {
    GenericByteList strColl = new GenericByteList();
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);
    strColl.add((byte)55);
    strColl.add((byte)56);
    strColl.add((byte)57);
    strColl.add((byte)58);

    IByteList searchable = strColl;
    final byte[] removedOccurances = searchable.toArray();

    // Hack For String Interning blah blah blah
    assertThat(removedOccurances.length).isEqualTo(searchable.size());
    assertThat(removedOccurances[0]).isEqualTo(searchable.get(0));
    assertThat(removedOccurances[1]).isEqualTo(searchable.get(1));
    assertThat(removedOccurances[2]).isEqualTo(searchable.get(2));
    assertThat(removedOccurances[3]).isEqualTo(searchable.get(3));
    assertThat(removedOccurances[4]).isEqualTo(searchable.get(4));
    assertThat(removedOccurances[5]).isEqualTo(searchable.get(5));
    assertThat(removedOccurances[6]).isEqualTo(searchable.get(6));
    assertThat(removedOccurances[7]).isEqualTo(searchable.get(7));
  }
}
