package com.base2art.lang;

import com.base2art.collections.IStringCollection;
import static org.fest.assertions.Assertions.*;
import org.junit.Test;

public class StringUtilTest
{

  @Test
  public void shouldSplit()
  {
    final IStringCollection coll = StringUtil.split("H.e.l.l.o", '.');
    assertThat(coll.size()).isEqualTo(5);
    assertThat(coll.get(0)).isEqualTo("H");
    assertThat(coll.get(1)).isEqualTo("e");
    assertThat(coll.get(2)).isEqualTo("l");
    assertThat(coll.get(3)).isEqualTo("l");
    assertThat(coll.get(4)).isEqualTo("o");
    assertThat(coll.first()).isEqualTo("H");
    assertThat(coll.last()).isEqualTo("o");

    assertThat(coll.join("--")).isEqualTo("H--e--l--l--o");
    assertThat(coll.join(null)).isEqualTo("Hello");
    assertThat(coll.join("")).isEqualTo("Hello");
  }

  @Test
  public void shouldCollapseDuringSplitForMultipleConnectedTokens()
  {
    final IStringCollection coll = StringUtil.split("H.e.l....l.o", '.');
    assertThat(coll.size()).isEqualTo(5);
    assertThat(coll.get(0)).isEqualTo("H");
    assertThat(coll.get(1)).isEqualTo("e");
    assertThat(coll.get(2)).isEqualTo("l");
    assertThat(coll.get(3)).isEqualTo("l");
    assertThat(coll.get(4)).isEqualTo("o");

    assertThat(coll.join("--")).isEqualTo("H--e--l--l--o");
    assertThat(coll.join(null)).isEqualTo("Hello");
    assertThat(coll.join("")).isEqualTo("Hello");
    assertThat(coll.reverse().join("")).isEqualTo("olleH");
  }


  @Test
  public void shouldHaveValueTests()
  {
    assertThat(StringUtil.hasValue(null)).isFalse();
    assertThat(StringUtil.hasValue("")).isFalse();
    assertThat(StringUtil.hasValue("a")).isTrue();
    assertThat(StringUtil.hasValue("A")).isTrue();
    assertThat(StringUtil.hasValue("Z")).isTrue();
    assertThat(StringUtil.hasValue("z")).isTrue();
    assertThat(StringUtil.hasValue("0")).isTrue();
    assertThat(StringUtil.hasValue("9")).isTrue();
    assertThat(StringUtil.hasValue("-")).isTrue();
    assertThat(StringUtil.hasValue("$")).isTrue();
    assertThat(StringUtil.hasValue("^")).isTrue();
    assertThat(StringUtil.hasValue(" ")).isFalse();
    assertThat(StringUtil.hasValue("\n")).isFalse();
    assertThat(StringUtil.hasValue("\r")).isFalse();
    assertThat(StringUtil.hasValue("\t")).isFalse();
    assertThat(StringUtil.hasValue(" \n\t\r")).isFalse();
  }

  @Test
  public void shouldSplitWordsSucessfully()
  {
    IStringCollection words;

    words = StringUtil.words("AbcAbcAbc");
    shouldEqual(new String[] { "Abc", "Abc", "Abc" }, words);

    words = StringUtil.words("abcAbcAbc");
    shouldEqual(new String[] { "abc", "Abc", "Abc" }, words);

    words = StringUtil.words("abc-Abc_Abc");
    shouldEqual(new String[] { "abc", "Abc", "Abc" }, words);

    words = StringUtil.words("abc");
    shouldEqual(new String[] { "abc"}, words);

    words = StringUtil.words("abC");
    shouldEqual(new String[] { "ab", "C"}, words);

    words = StringUtil.words("abc-abc_abc");
    shouldEqual(new String[] { "abc", "abc", "abc" }, words);

    words = StringUtil.words("abc:abc3:abc");
    shouldEqual(new String[] { "abc", "abc3", "abc" }, words);

    words = StringUtil.words("abc abc3    abc");
    shouldEqual(new String[] { "abc", "abc3", "abc" }, words);
    shouldEqual(new String[] { "abc", "abc3", "abc" }, words);
  }

  private void shouldEqual(String[] expected, IStringCollection actual)
  {
    assertThat(expected.length).isEqualTo(actual.size());
    for (int i = 0; i < expected.length; i++)
    {
      assertThat(expected[i]).isEqualTo(actual.get(i));
    }
  }
}