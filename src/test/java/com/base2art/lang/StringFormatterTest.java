package com.base2art.lang;

import com.base2art.collections.IStringCollection;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class StringFormatterTest
{

  @Test
  public void shouldFormatWords()
  {
    assertThat("aa1_bb1_cc1").isEqualTo(StringFormatter.format("Aa1Bb1Cc1", StringFormatType.Underscore));
    assertThat("Aa1Bb1Cc1").isEqualTo(StringFormatter.format("Aa1Bb1Cc1", StringFormatType.Pascal));
    assertThat("Aa1Bb1Cc1").isEqualTo(StringFormatter.format("Aa1Bb1_cc1", StringFormatType.Pascal));
    assertThat("Aa1Bb1Cc1").isEqualTo(StringFormatter.format("aa1_Bb1-cc1", StringFormatType.Pascal));
    assertThat("aa1Bb1Cc1").isEqualTo(StringFormatter.format("Aa1_bb1  cc1", StringFormatType.Camel));
    assertThat("aa1-bb1-cc1").isEqualTo(StringFormatter.format("Aa1_bb1  cc1", StringFormatType.Dash));
    assertThat("Aa1 Bb1 Cc1").isEqualTo(StringFormatter.format("Aa1_bb1  cc1", StringFormatType.TitleWords));
    assertThat("Aa1 bb1 Cc1").isEqualTo(StringFormatter.format("Aa1_bb1  Cc1", StringFormatType.Words));
  }
}