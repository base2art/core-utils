package com.base2art.lang;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class ObjectUtilTest
{

  @Test
  public void shouldSomething()
  {
    assertThat(ObjectUtil.as(this, ObjectUtilTest.class)).isEqualTo(this);
    assertThat(ObjectUtil.as(this, String.class)).isEqualTo(null);
  }
}