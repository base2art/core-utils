package com.base2art.util;

import org.junit.Test;
import static org.fest.assertions.Assertions.assertThat;

public class SingleSetValueContainerTest
{

  @Test
  public void shouldStayNullUponSet()
  {
    IValueContainer<String> container = new SingleSetValueConainer<String>();

    container.setValue(null);
    assertThat(container.getValue()).isNull();

    container.setValue("Hello");
    assertThat(container.getValue()).isNull();

    container.setValue("World");
    assertThat(container.getValue()).isNull();
  }

  @Test
  public void shouldStayValueUponSet()
  {
    IValueContainer<String> container = new SingleSetValueConainer<String>();

    container.setValue("Hello");
    assertThat(container.getValue()).isEqualTo("Hello");

    container.setValue("World");
    assertThat(container.getValue()).isEqualTo("Hello");

    container.setValue(null);
    assertThat(container.getValue()).isEqualTo("Hello");
  }


  @Test
  public void shouldNotStayNullUponSet()
  {
    IValueContainer<String> container = new SingleSetValueConainer<String>(true);

    container.setValue(null);
    assertThat(container.getValue()).isNull();
    
    container.setValue("Hello");
    assertThat(container.getValue()).isEqualTo("Hello");

    container.setValue("World");
    assertThat(container.getValue()).isEqualTo("Hello");

    container.setValue(null);
    assertThat(container.getValue()).isEqualTo("Hello");
  }
}