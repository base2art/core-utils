package com.base2art.util.annotations;

import com.base2art.annotations.DefaultImpl;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class TestImplTest
{

  @Test
  public void shouldFindTest()
  {
    Class<ISimpleInterface> inter = ISimpleInterface.class;
    DefaultImpl annotation = inter.getAnnotation(DefaultImpl.class);
    assertThat(annotation.value()).isEqualTo(SimpleInterface.class);
  }
}
