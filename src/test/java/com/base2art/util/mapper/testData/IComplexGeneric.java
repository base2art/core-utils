package com.base2art.util.mapper.testData;

public interface IComplexGeneric<T1, T2>
{
  T1 getGenericValue();

  T2[] getGenericValues();

  Object[] getObjectArrayValues();
}
