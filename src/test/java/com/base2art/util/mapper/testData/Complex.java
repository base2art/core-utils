package com.base2art.util.mapper.testData;

import java.util.UUID;

public class Complex implements IComplex
{

  private int intValue;

  @Override
  public int getIntValue()
  {
    return intValue;
  }

  public void setIntValue(int intValue)
  {
    this.intValue = intValue;
  }
  private String genValue;

  @Override
  public String getGenericValue()
  {
    return this.genValue;
  }

  public void setGenericValue(String genValue)
  {
    this.genValue = genValue;
  }
  private UUID[] genValues;

  @Override
  public UUID[] getGenericValues()
  {
    return this.genValues;
  }

  public void setGenericValues(UUID[] genValues)
  {
    this.genValues = genValues;
  }
  private Object[] objectArrayValues;

  @Override
  public Object[] getObjectArrayValues()
  {
    return this.objectArrayValues;
  }

  public void setObjectArrayValues(Object[] objectArrayValues)
  {
    this.objectArrayValues = objectArrayValues;
  }
  private String[] stringValues;

  public String[] getStringValues()
  {
    return stringValues;
  }

  public void setStringValues(String[] stringValues)
  {
    this.stringValues = stringValues;
  }
}
