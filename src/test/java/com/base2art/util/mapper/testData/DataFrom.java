package com.base2art.util.mapper.testData;

import java.util.UUID;

public class DataFrom implements IDataFrom
{

  private UUID id;
  private String stringValue;
  private int intValue;
  private Integer integerValue;
  private UUID uuidValue;
  private IComplex complexValue;
  private IComplex[] complexItemsValue;
  private String[] stringValues;

  @Override
  public UUID getId()
  {
    return id;
  }

  public void setId(UUID id)
  {
    this.id = id;
  }

  @Override
  public int getIntValue()
  {
    return this.intValue;
  }

  public void setIntValue(int intValue)
  {
    this.intValue = intValue;
  }

  @Override
  public Integer getIntegerValue()
  {
    return this.integerValue;
  }

  public void setIntegerValue(Integer integerValue)
  {
    this.integerValue = integerValue;
  }

  @Override
  public String getStringValue()
  {
    return this.stringValue;
  }

  public void setStringValue(String stringValue)
  {
    this.stringValue = stringValue;
  }

  @Override
  public UUID getUUIDValue()
  {
    return this.uuidValue;
  }

  public void setUUIDValue(UUID uuidValue)
  {
    this.uuidValue = uuidValue;
  }

  @Override
  public void getVoid()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public IComplex getComplex()
  {
    return this.complexValue;
  }

  public void setComplexValue(IComplex complexValue)
  {
    this.complexValue = complexValue;
  }

  @Override
  public IComplex[] getComplexItems()
  {
    return this.complexItemsValue;
  }

  public void setComplexItemsValue(IComplex[] complexItemsValue)
  {
    this.complexItemsValue = complexItemsValue;
  }

  public String[] getStringValues()
  {
    return stringValues;
  }

  public void setStringValues(String[] stringValues)
  {
    this.stringValues = stringValues;
  }
}
