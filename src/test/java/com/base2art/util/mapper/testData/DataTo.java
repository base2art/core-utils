package com.base2art.util.mapper.testData;

import java.util.UUID;

public class DataTo
{

  private String stringValue;
  private int intValue;
  private Integer integerValue;
  private UUID uuidValue;
  private ComplexData complex;
  private ComplexData[] complexItems;
  private String[] stringValues;

  public int getIntValue()
  {
    return this.intValue;
  }

  public void setIntValue(int intValue)
  {
    this.intValue = intValue;
  }

  public Integer getIntegerValue()
  {
    return this.integerValue;
  }

  public void setIntegerValue(Integer integerValue)
  {
    this.integerValue = integerValue;
  }

  public String getStringValue()
  {
    return this.stringValue;
  }

  public void setStringValue(String stringValue)
  {
    this.stringValue = stringValue;
  }

  public UUID getUUIDValue()
  {
    return this.uuidValue;
  }

  public void setUUIDValue(UUID uuidValue)
  {
    this.uuidValue = uuidValue;
  }

  public ComplexData getComplex()
  {
    return complex;
  }

  public void setComplex(ComplexData complex)
  {
    this.complex = complex;
  }

  public ComplexData[] getComplexItems()
  {
    return complexItems;
  }

  public void setComplexItems(ComplexData[] complexItems)
  {
    this.complexItems = complexItems;
  }

  public String[] getStringValues()
  {
    return stringValues;
  }

  public void setStringValues(String[] stringValues)
  {
    this.stringValues = stringValues;
  }
}
