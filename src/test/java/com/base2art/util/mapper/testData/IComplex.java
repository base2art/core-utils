package com.base2art.util.mapper.testData;

import java.util.UUID;

public interface IComplex extends IComplexGeneric<String, UUID>
{

  int getIntValue();

  String[] getStringValues();
}
