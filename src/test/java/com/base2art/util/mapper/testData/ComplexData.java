package com.base2art.util.mapper.testData;

import java.util.UUID;

public class ComplexData
{

  private int intValue;

  public int getIntValue()
  {
    return intValue;
  }

  public void setIntValue(int intValue)
  {
    this.intValue = intValue;
  }
  private String genValue;

  public String getGenericValue()
  {
    return this.genValue;
  }

  public void setGenericValue(String genValue)
  {
    this.genValue = genValue;
  }
  private UUID[] genValues;

  public UUID[] getGenericValues()
  {
    return this.genValues;
  }

  public void setGenericValues(UUID[] genValues)
  {
    this.genValues = genValues;
  }
  private Object[] objectArrayValues;

  public Object[] getObjectArrayValues()
  {
    return this.objectArrayValues;
  }

  public void setObjectArrayValues(Object[] objectArrayValues)
  {
    this.objectArrayValues = objectArrayValues;
  }
  private String[] stringValues;

  public String[] getStringValues()
  {
    return stringValues;
  }

  public void setStringValues(String[] stringValues)
  {
    this.stringValues = stringValues;
  }
}
