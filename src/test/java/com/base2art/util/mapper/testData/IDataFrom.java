package com.base2art.util.mapper.testData;

import java.util.UUID;

public interface IDataFrom
{

  UUID getId();

  String getStringValue();

  int getIntValue();

  Integer getIntegerValue();

  UUID getUUIDValue();

  void getVoid();

  IComplex getComplex();

  IComplex[] getComplexItems();

}
