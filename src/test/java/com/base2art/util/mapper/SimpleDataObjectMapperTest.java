package com.base2art.util.mapper;

import com.base2art.util.mapper.testData.*;
import java.util.UUID;
import static org.fest.assertions.Assertions.*;
import org.junit.Test;

public class SimpleDataObjectMapperTest
{

  @Test
  public void shouldSomething()
  {
    SimpleDataObjectMapper<DataFrom, DataTo> mapper = new SimpleDataObjectMapper<DataFrom, DataTo>(DataFrom.class, DataTo.class);
    mapper.addMapping(IComplex.class, ComplexData.class);
    mapper.exclude("Id");

    final UUID uuid = UUID.randomUUID();
    DataFrom fromObj = new DataFrom();
    fromObj.setIntValue(1);
    fromObj.setIntegerValue(2);
    fromObj.setStringValue("Leat");
    fromObj.setUUIDValue(uuid);
    final Complex complex = new Complex();
    complex.setIntValue(666);
    complex.setStringValues(new String[]{"a", "b", "c"});
    fromObj.setComplexValue(complex);
    fromObj.setStringValues(new String[]
      {
        "1", "2", "3"
      });


    final Complex complex2 = new Complex();
    complex2.setIntValue(99);
    fromObj.setComplexItemsValue(new IComplex[]
      {
        complex, complex2
      });


    DataTo toObj = new DataTo();
    mapper.map(fromObj, toObj);

    assertThat(toObj.getIntValue()).isEqualTo(fromObj.getIntValue());
    assertThat(toObj.getStringValue()).isEqualTo(fromObj.getStringValue());
    assertThat(toObj.getUUIDValue()).isEqualTo(fromObj.getUUIDValue());
    assertThat(toObj.getIntegerValue()).isEqualTo(fromObj.getIntegerValue());
    assertThat(toObj.getComplex().getIntValue()).isEqualTo(666);
    assertThat(toObj.getComplexItems().length).isEqualTo(2);
    assertThat(toObj.getComplexItems()[0].getIntValue()).isEqualTo(666);
    assertThat(toObj.getComplexItems()[1].getIntValue()).isEqualTo(99);
    assertThat(toObj.getStringValues().length).isEqualTo(3);
    assertThat(toObj.getStringValues()[2]).isEqualTo("3");
  }
}