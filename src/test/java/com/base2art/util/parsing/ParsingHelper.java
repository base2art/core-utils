package com.base2art.util.parsing;

import com.base2art.util.Option;
import java.util.NoSuchElementException;
import static org.fest.assertions.Assertions.assertThat;

public class ParsingHelper
{

  public static <T> void assertValue(IParser<T> parser, String input, T expected, T orElse)
  {
    final Option<T> parsedValue = parser.tryParse(input);

    boolean shouldBeEmpty = expected == null;
    final T orElseExpected = shouldBeEmpty ? orElse : expected;
    assertThat(parsedValue.getOrElse(orElse)).isEqualTo(orElseExpected);

    boolean hasException = false;
    try
    {
      assertThat(parsedValue.get()).isEqualTo(expected);
    }
    catch (NoSuchElementException e)
    {
      hasException = true;
    }

    assertThat(hasException).isEqualTo(shouldBeEmpty);
    assertThat(parsedValue.isNone()).isEqualTo(shouldBeEmpty);

  }
}
