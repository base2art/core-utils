package com.base2art.util.parsing;

import org.junit.Test;

public class IntegerParserTest
{

  @Test
  public void shouldSomething()
  {
    IntegerParser intParser = new IntegerParser();
    ParsingHelper.<Integer>assertValue(intParser, null, null, 0);
    ParsingHelper.<Integer>assertValue(intParser, null, null, 1);
    ParsingHelper.<Integer>assertValue(intParser, "", null, 0);
    ParsingHelper.<Integer>assertValue(intParser, "NotParsible", null, 0);
    ParsingHelper.<Integer>assertValue(intParser, "1", 1, 0);
  }
}