package com.base2art.util.reflect;

import com.base2art.annotations.DefaultImpl;

@DefaultImpl(TestReflectObject.class)
public interface ITestReflectObject
{

  String instanceMethod();

  String instanceMethodWithArgs(int i);
}
