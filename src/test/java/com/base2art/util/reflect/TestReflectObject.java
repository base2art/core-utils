package com.base2art.util.reflect;

public class TestReflectObject
  implements ITestReflectObject
{

  public static String staticField = "StaticFieldValue";
  public String instanceField = "FieldValue";

  @Override
  public String instanceMethod()
  {
    return "MethodValue";
  }

  public static String staticMethod()
  {
    return "StaticMethodValue";
  }

  @Override
  public String instanceMethodWithArgs(int i)
  {
    return "MethodValueWithArgs:" + i;
  }

  public static String staticMethodWithArgs(int i)
  {
    return "StaticMethodValueWithArgs:" + i;
  }
}
