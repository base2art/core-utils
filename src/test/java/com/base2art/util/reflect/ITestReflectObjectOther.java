package com.base2art.util.reflect;

public interface ITestReflectObjectOther {

  String instanceMethod();

  String instanceMethodWithArgs(int i);

}
