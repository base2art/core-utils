package com.base2art.util.reflect;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class ReflectiveByClassNameTest
{

  @Test
  public void shouldLoadInstance()
  {
    final TestReflectObject instance = (TestReflectObject) klazz().createInstance();
    assertThat(instance).isNotNull();
    assertThat(instance.instanceMethodWithArgs(24)).isEqualTo("MethodValueWithArgs:24");
  }

  @Test
  public void shouldCallStaticMethod()
  {
    final String instance = klazz().<String>method("staticMethod").invoke();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("StaticMethodValue");
  }

  @Test
  public void shouldCallStaticMethodWithArgs()
  {
    final String instance = klazz().<String>method("staticMethodWithArgs").invokeWith(24);
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("StaticMethodValueWithArgs:24");
  }

  @Test
  public void shouldCallInstanceMethod()
  {
    final String instance = klazz().<String>methodOfNew("instanceMethod").invoke();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("MethodValue");
  }

  @Test
  public void shouldCallInstanceMethodWithArgs()
  {
    final String instance = klazz().<String>methodOfNew("instanceMethodWithArgs").invokeWith(25);
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("MethodValueWithArgs:25");
  }

  @Test
  public void shouldGetInstanceField()
  {
    final String instance = klazz().<String>fieldOfNew("instanceField").get();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("FieldValue");
  }

  @Test
  public void shouldGetStaticField()
  {
    final String instance = klazz().<String>field("staticField").get();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("StaticFieldValue");
  }

  private IReflectiveClass<?> klazz()
  {
    return Reflective.forClass(null, "com.base2art.util.reflect.TestReflectObject");
  }
}