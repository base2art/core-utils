package com.base2art.util.reflect;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class ReflectiveTest
{

  @Test
  public void shouldLoadInstance()
  {
    final TestReflectObject instance = klazz().createInstance();
    assertThat(instance).isNotNull();
    assertThat(instance.instanceMethodWithArgs(24)).isEqualTo("MethodValueWithArgs:24");
  }

  @Test
  public void shouldLoadInterfaceWithAnnotationInstance()
  {
    final IReflectiveClass<ITestReflectObject> klazz = Reflective.forClass(ITestReflectObject.class);
    final ITestReflectObject instance = klazz.createInstance();
    assertThat(instance).isNotNull();
    assertThat(instance.instanceMethodWithArgs(24)).isEqualTo("MethodValueWithArgs:24");
  }

  @Test
  public void shouldLoadInterfaceNoAnnotationInstance()
  {
    final IReflectiveClass<ITestReflectObjectOther> klazz = Reflective.forClass(ITestReflectObjectOther.class, true);
    assertThat(klazz.createInstance()).isNull();
  }

  @Test
  public void shouldLoadNonExistantTypeNoExceptions()
  {
    final IReflectiveClass<?> klazz = Reflective.forClass(
        ITestReflectObjectOther.class.getClassLoader(), 
        "java.FakeClass", 
        true);
    assertThat(klazz.method("NullMethod").invoke()).isNull();
  }

  @Test
  public void shouldCallStaticMethod()
  {
    final String instance = klazz().<String>method("staticMethod").invoke();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("StaticMethodValue");
  }

  @Test
  public void shouldCallStaticMethodWithArgs()
  {
    final String instance = klazz().<String>method("staticMethodWithArgs").invokeWith(24);
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("StaticMethodValueWithArgs:24");
  }

  @Test
  public void shouldCallInstanceMethod()
  {
    final String instance = klazz().<String>methodOfNew("instanceMethod").invoke();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("MethodValue");
  }

  @Test
  public void shouldGetReturnTypeOfMethodAndCreateInstance()
  {
    final String instance = klazz()
        .<String>methodOfNew("instanceMethodWithArgs")
        .returnTypeWith(0)
        .createInstance();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("");
  }

  @Test
  public void shouldCallInstanceMethodWithArgs()
  {
    final String instance = klazz().<String>methodOfNew("instanceMethodWithArgs").invokeWith(25);
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("MethodValueWithArgs:25");
  }

  @Test
  public void shouldGetInstanceField()
  {
    final String instance = klazz().<String>fieldOfNew("instanceField").get();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("FieldValue");
  }

  @Test
  public void shouldGetStaticField()
  {
    final String instance = klazz().<String>field("staticField").get();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("StaticFieldValue");
  }

  @Test
  public void shouldGetTypeOfFieldAndCreateInstance()
  {
    final String instance = klazz()
        .<String>field("staticField")
        .type()
        .createInstance();
    assertThat(instance).isNotNull();
    assertThat(instance).isEqualToIgnoringCase("");
  }

  private IReflectiveClass<TestReflectObject> klazz()
  {
    return Reflective.forClass(TestReflectObject.class);
  }
}
